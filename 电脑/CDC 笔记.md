# CDC 笔记



## 一、名词介绍

**CDC （Change data capture）变化数据捕获**

一个简单和实时的解决方案，用于在需要时持续摄取和复制企业数据

Qlik 将数据从一个源实时复制到另一个目标，通过一个简单的图形界面进行管理，它完全自动化了端到端复制。通过精简和无代理的配置，数据工程师可以基于领先的变化数据捕获（CDC）技术轻松地设置、控制和监控数据管道。

**Qlik Replicate**

通用的数据复制和数据摄取工具



## 二、Qlik Replicate 使用指南



### 2.1 Manage Endpoint Connections 管理端点的连接

这里定义源头端和目标端，支持多种类型，诸如 Oracle、Microsoft SQL Server 等
参数包括：名称、描述、角色（source 源头端 / target 目标端）、类型、连接表达式、用户名、密码

详细源头端支持类型：https://help.qlik.com/en-US/replicate/May2023/Content/Replicate/Main/Support%20Matrix/supported_source_endpoints.htm#ar_app_supported_dbversions_993761142_1330908
详细目标端支持类型：https://help.qlik.com/en-US/replicate/May2023/Content/Replicate/Main/Support%20Matrix/supported_target_endpoints.htm#ar_app_supported_dbversions_993761142_1341803

### 2.2 Task 复制任务

通过拖曳的方法，设定源头端和目标端，然后选择数据表（默认是全表，支持过滤器设置只复制部分数据）

创建任务后，由于任务尚未设置，该选项卡将在 Designer 设计者视图中打开。

#### 2.2.3 designer 视图

左边是源头端和目标端的设定，通过拖曳的方式添加到该任务的相应位置上，橙色向外图标代表源头端，向外传送数据，蓝色向内图标代表目标端，接收从外部的数据。

通过右边的 Table Selection （表选择）选择数据表，选择 Schema （数据库/用户），Table 栏可以搜索，在 Table List （表列表）把需要复制数据的表，用 箭头按钮，选择到 Selected Tables （已选表）里。

#### 2.2.4 monitor view  任务监视器视图

Full load tab  指示满载过程中的状态进度
Change processing tab  监视满载完成后发生的更改。

点击 Select All 链接，在图形下方显示一个表，其中包含任务中正在处理的每个表的信息。

单击各个条形图，如Completed图和Loading图，以查看其他信息。

#### 2.2.4.1 Full load 满载

包括以下信息：

- Total Completion 完全加载总完成栏：显示加载到目标终结点的所有记录的进度。
- Tables 状态栏：指示正在加载的表的状态。
  - Completed 完整的：完成加载到目标终结点的表数。
  - Loading 加载中的：正在加载到目标端点的表数。
  - Queued 排队的：等待加载到目标终结点的表数。
  - Error 错误的：由于错误而无法加载的表数。
- Throughput 吞吐量：显示当前吞吐量。吞吐量的数字代表指定时间段内任务中读取的事件数。

点击任意的项目，下方会显示详细信息

##### 2.2.4.1.1 Total Completion 详细信息

此表显示以下进度详细信息：

| Detail 细节 | Total 总计                     | Completed 完整的           | Remaining 剩下的     | Notes 备注 |
| ----------- | ------------------------------ | -------------------------- | -------------------- | ---------- |
| Tables      | 任务中包含的表总数             | 当前时间完成加载的表总数   | 等待加载的表总数     | 其他信息   |
| Records     | 当前时间完成加载的总记录       | 当前时间完成加载的记录总数 | 等待加载的记录总数   | 其他信息   |
| Time        | 加载任务中所有选定表的估计时间 | 总运行时间                 | 加载其余表的估计时间 | 其他信息   |

##### 2.2.4.1.2 Tables 任务中每个表的信息



