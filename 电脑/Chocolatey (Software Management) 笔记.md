# Chocolatey (Software Management) 笔记



## 使用方法



choco [command] [options]

**安装程序包**
choco install  package_name [package_name...]

**卸载程序包**
choco uninstall  package_name [package_name...]

**搜索程序包**
choco find  keyword

**查询程序包信息**
choco info  package_name



**选项：**

-?, --help, -h  输出帮助信息
-d, --debug  调试模式
-v, --verbose  输出详细信息
-y, --yes, --confirm  确认所有信息，不进行任何提醒

