# GaussDB 笔记



## 数据表



**查表结构**

参考文档：https://support.huaweicloud.com/devg-dws/dws_04_0653.html

select t1.table_name, t1.column_name, t1.data_type, t1.data_length, t1.nullable, t2.comments

from all_tab_columns t1

left join all_col_comments t2 on t1.table_name = t2.table_name and t1.column_name = t2.column_name

where t1.table_name in ('')

order by t1.table_name, t1.column_id;



**查表结构定义**

select * from pg_get_tabledef('szc_sj_sjzl.ele_swjg')



## 客户端命令使用

gsql [option] [dbName [userName]]

-h hostname
-p port   default: 5432
-U username  default: root
-W password
-f  执行SQL文件



## gsql 命令

\h  帮助
\q  退出
\d+ table_name   查表结构







