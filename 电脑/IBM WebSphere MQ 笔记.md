# IBM WebSphere MQ 笔记

## 命令行管理工具

启动管理工具

runmqsc QMgrName

其中 QMgrName 是MQ队列管理器的名字

进入管理器后就可以使用 MQSC 命令，输入问号(?)，即可显示所有可用的一级命令，命令不区分大小写。

MQSC 命令的基本语法格式都是由动作（粤糟贼蚤燥灶） 垣 对象（韵遭躁藻糟贼） 垣 参数（孕葬则葬皂藻贼藻则）组成。

输入一级命令+问号(?)，即可查询一级命令的参数，例如：

display ?

### display 命令

display 命令显示对象的信息

**display channel(channel_name)**

显示一个通道(channel)的基本信息，其中 channel_name 替换为实际通道名

**display channel(channel_name) HBINT**

显示一个通道(channel)的心跳间隔，其中 channel_name 替换为实际通道名

**display queue(queue_name)**

显示一个队列(queue)的基本信息，其中 queue_name 替换为实际队列名

**display chstatus(channel_name)**

显示一个通道(channel)的状态信息(status)，其中 channel_name 替换为实际通道名

**display qstatus(queue_name)**

显示一个队列(queue)的状态信息(status)，其中 queue_name 替换为实际队列名




