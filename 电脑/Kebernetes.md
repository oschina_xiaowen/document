# Kebermetes



## 一、简介

Kebermetes (简称K8s) 是目前最受欢迎的容器编排工具之一。它提供了一个强大的平台，用于自动部署、扩展和管理容器化应用程序。

Kebermetes 具有以下特点：

1. 自动化部署，可以通过定义清单文件 (如yaml文件) 来描述应用程序的部署要求,Kebermetes 会自动处理容器的调度和部署过程.
2. 弹性伸缩，Kebermetes 可以根据负载自动扩展应用程序的实例数量,以满足用户对应用程序的需求。
3. 自我修复，当容器实例发生故障时，Kebernetes 可以自动替换失败的实例，确保应用程序的高可用性。
4. 服务发现和负载均衡，Kebernetes 提供了内置的服务发现和负载均衡机制，使得容器之间可以轻松通讯，并且自动分配流量。



## 二、Kebernetes 架构

部署一个应用程序就是部署一个 Kebernetes 集群，一个集群包含2个角色，master 和 node。

**master** 节点在一个集群里面只有1个，负责控制所有工作节点 node 的工作。

**node** 节点是运行应用程序实例的节点。

master 节点组件：

1. API Server，用于接受响应所有的请求
2. Scheduler，集群调度器，负责决定将 Pod 放在哪个 node 上运行
3. Controller Manager，负责管理集群内各种资源
4. Etcd，Kubernetes 数据库，用于存储集群的配置信息和各种资源的状态信息

node 节点组件：

1. kubelet，运行在 node 节点上的 agent，用于调用容器引擎创建 pod，并向 master 报告运行状态
2. Kube-proxy，网络负载均衡组件，维护着 node 节点上的网络规则
3. docker，容器引擎

**Namespace**

Namespace 是 Kubernetes 中用于将集群划分为多个虚拟集群的一种机制。它提供了一种将资源隔离开的方式，使得在同一个集群中可以运行多个互相独立的应用程序或服务。

**Pod**

Pod 是 Kubernetes 中最小的可部署的独立单位。

Pod 包含一个或多个业务容器以及它们的共享资源。

一个 Pod 中包含至少两个容器，其中一个为 pause 容器，用来承载 pod 的网络，剩余容器用来承载真正的业务。

**Deployment**

Deployment 用于管理无状态的应用程序，例如 Web 服务器或者 API 服务。Deployment 会创建指定数量的 Pod 副本，并确保这些 Pod 在集群中均匀分布。当需要更新应用程序时，Deployment 可以通过滚动更新的方式逐步替换旧的 Pod 副本，以确保应用程序的高可用性。

**StatefulSet**

5tatefulSet 用于管理有状态的应用程序，例如数据库或者缓存系统。StatefulSet 会为每个 Pod 分配唯一的标识符，使得每个 Pod 可以在重新部署时保持相同的标识符。这可以确保应用程序的状态在重新部署时得以保留，并且可以保证Pod的启动顺序。

**DaemonSet**

DaemonSet 是 Kubernetes 中的一种控制器对象，它的主要作用是在 Kubernetes 集群的每个节点上运行一个 Pod 的副本。这意味着当 DaemonSet 被创建时，系统会自动调度 Pod 到所有符合条件的节点上，确保每个节点上都有且仅有一个该 Pod 的实例。

**Service**

引入 Service 主要是解决 Pod 的动态变化，通过创建 Service，可以为一组具有相同功能的容器应用提供一个统一的入口地址，并且将请求负载分发到后端的各个容器应用上。

Service 在很多情况下只是一个概念，真正起作用的其实是 kube-proxy 服务进程，每个 Node 节点上都运行着一个 kube-proxy 服务进程。

**Ingress**

在 Kubernetes 集群中，Ingress 作为集群内服务对外暴露的访问接入点，几乎承载着集群内服务访问的所有流量。
Ingress 资源仅支持配置HTTP流量的规则。

**PV 和 PVC**

由于k8s支持的存储系统有很多，全部掌握过于团难。为了能够屏蔽底层存储实现的细节，方便用户使用，k8s引入PV和PVC两种资源对象。

PV[Persistent Volume]是持久化卷的意思，是对底层的共享存储的一种抽象。一般情况下PV由k8s管理员进行创建和配置，它与底层具体的共享存储技术有关，并通过插件完成与共享存储的对接。

PVC[Persistent Volume Claim]是持久卷声明的意思，是用户对于存储需求的一种声明。换句话说，PVC其实就是用户向k8s系统发出的一种资源需求申请。

