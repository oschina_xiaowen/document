# Linux 发行版分支

## 两大家族

* Red Hat 家族
  
  * Fedora
    
    * RHEL (Red Hat Enterprise Linux): 面向企业
      
      * CentOS: 面向社区
      
      * Oracle Enterprise Linux (OEL): 面向企业

* Debian 家族
  
  * Debian
    
    * Ubuntu Desktop: Ubuntu 桌面版本，包含一些办公软件、多媒体软件和浏览器
    
    * Ubuntu Server: 用来做服务器，不包含桌面系统
    
    * Chromium OS / Google Chrome OS


