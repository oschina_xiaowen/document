# Linux 笔记



## 一、系统目录

文件系统层次结构标准 (Filesystem Hierarchy Standard，FHS) 定义了在类Unix系统中的目录结构和目录内容。

**启动文件**
/boot  包含引导加载程序相关的文件

**指令集合**
/bin  binary，存放二进制程序
/sbin  super binary，存放超级管理的二进制程序，只有 root 用户才能执行

**设备**
/dev  device 简写，存放设备文件
/media  即插即用的外部设备、如 U 盘
/mnt  mount 的简写，临时挂载的文件系统，如网络硬盘 nfs

**配置**
/etc  Editable Text Configuration 的简写，存放配置文件

**临时文件**
/run  存储系统启动以来的信息
/tmp  存放一些临时文件

**用户目录**
/home  子目录以用户名命名
/root  相当于 root 用户的 home 目录，独立出来
/usr  用户安装的应用程序，下级有 bin、etc 等目录

**数据目录**
/var  存放数据，如 log 存放日志文件
/proc  processes 的简写，(该目录不存在硬盘上) 将进程和内核信息以文件形式呈现的虚拟文件系统

**第三方应用程序**
/opt  Optional application software packages 取 Optional 头3个字母



## 二、常用软件

| 常用 GUI 软件 | 说明                               | 常用程序包      | 说明                           |
| ------------- | ---------------------------------- | --------------- | ------------------------------ |
| GParted       | 分区管理器                         | nano            | 文本编辑器                     |
| Wine          | Windows 程序模拟器，图标是一杯红酒 | telnet          | 测试网络端口                   |
| Firefox       | 网页浏览器                         | tcping          | 测试 TCP 网络端口              |
| Chromium      | 网页浏览器                         | iotop           | 查看 I/O 情况                  |
| Audacious     | 音频播放器                         | traceroute      | 路由追踪工具                   |
| VLC           | 多媒体播放器                       | cups-pdf        | PDF 打印机                     |
| LibreOffice   | 办公软件                           | python-dev      | python                         |
| Wireshark     | 网络嗅探器                         | python-pip      | python 包管理工具              |
|               |                                    | kde-desktop     | KDE 桌面环境                   |
|               |                                    | kde-full        | KDE 完整软件库                 |
|               |                                    | xfce4           | XFCE 桌面程序                  |
|               |                                    | gnome-commander | GNOME 桌面快速强大的文件管理器 |
|               |                                    | gnome-nettool   | GNOME 桌面网络工具             |
|               |                                    | mailx           | 邮件发送工具                   |
|               |                                    | OpenJDK         | Java 虚拟机                    |



## 三、命令行基础用法

**自动补全路径**

按 Tab 键可以自动补全路径，比如：

/v  {Tab}  =>  /var
/usr/lo  {Tab}  =>  /usr/local

**快捷键**

Ctrl+A  光标跳到行头
Ctrl+E  光标跳到行尾（end）
Ctrl+U  清除光标处到左边的内容
Ctrl+K  清除光标处到右边的内容
Ctrl+左箭头  光标跳到前一个单词的开头
Ctrl+右箭头  光标跳到前一个单词的末尾
Ctrl+W  删除光标左边的单词（word）
Ctrl+L  清屏
Ctrl+R  从历史记录中搜索
Alt+# (Alt+Shift+3)  注释执行，相当于依次按 Ctrl+A, #, Enter，当前命令不会执行且会添加到 history 里，非常适合输入到一半但又想起还不能执行该步骤的情况

**通配符**

| 符号 | 含义                      |
| ---- | ------------------------- |
| *    | 0个或多个字符组成的字符串 |
| ?    | 任何1个字符               |
| []   | 与正则表达式一样          |

例如：ls -l f*，将输出 f 字母开头的文件

**命令行历史**

history

叹号+序号，可以执行历史记录，如：
!20

**管道符号**

命令1执行的输出结果作为命令2的参数
命令1 | 命令2 ...

输出内容超过一页可以用 more
service --help | more

**箭头符号**

echo "content" > /path/to/file
输出内容到文件，覆盖原有内容

echo "" > FILE
写入空白内容到一个文件，可以用于清空某个文件。

echo "content" >> /path/to/file.log
输出内容到文件，追加原有内容，日志文件常用操作

**参数使用动态内容**

使用 $() 包含要执行的命令，如果是在字符串里面插入，则还需要用反引号"`"包含。

```Shell
# 输出动态内容到 today_yyyymmdd.log 动态命名的文件
echo $(date +%Y%m%d) > today_`$(echo date +%Y%m%d)`.log
```

**多行输出结果转为一行**

paste -s
默认分隔符为制表符
-d  参数：delimiters，指定分隔符

```shell
# 把找到的文件按空格分隔合为一行
find ./ -name '*.log' | paste -s -d ' '
```



**显示或设置shell特性及shell变量**

set [options] [params]
-x：执行指令后，会先显示该指令及所下的参数，用于调试输出
-u：当执行时使用到未定义过的变量，则显示错误信息。



## 四、系统基础功能



### 4.1 文件系统操作

**cd (change directory) 变更目录**

cd /  切换到 / 根目录

cd ~  切换到 home 目录，相当于 cd $home

cd ..  返回上一级目录

**pwd (print work directory) 输出当前工作目录路径**

pwd

**list 列出目录内容**

ls [OPTION] [path]

-l  list 列出详细信息
-h human readable 文件尺寸按照人性化可读性显示，如1K，234M，2G
-a  all 列出所有文件，包括以 . 开头的

第一列是这个文件的属性，一共11位字符
第一位表示这个文件的性质，d 代表目录，- 代表文件，l 代表链接文件（link file），b 代表设备文件中可供存储的接口设备，c 代表设备文件中的串行端口设备，例如键盘、鼠标。
第二位开始，每3位为一组，分别表示 user，group，other 的权限（r 代表 read, w 代表 write, x 代表 execute）。
第二列表示连接占用的节点（i-node）
第三列表示这个文件或目录的“拥有者”
第四列表示拥有者的用户组
第五列为这个文件的大小
第六列为这个文件的创建日期或者是最近的修改日期
第七列为这个文件的文件名：如果文件名前面有.说明是隐藏文件。

**mv (move) 移动/重命名文件**  
mv [OPTION] &lt;SOURCE&gt; &lt;DEST&gt;  

-f force 不需要询问强制覆盖写入

**cp (copy) 拷贝文件**..
cp [OPTION] SOURCE DEST

-f force 不需要询问强制覆盖写入  
-R recursive 递归拷贝，包括所有子目录（默认忽略子目录）  
-v verbose 显示详细信息

**more 查看文件内容**

more filename

逐行查看，按回车键转到下一行，按 q 退出。

**head 查看文件的开头部分的内容**

head -n 10 filename

-n 代表显示的行数，默认为10

**tail 查看文件内容最后N行，常用于查看不断变化的日志文件**

tail -fn 100 filename

-f 代表循环读取，文件一旦有更新，就输出新内容出来

-n 代表显示的行数，默认为10

**nano 编辑文件内容**

nano [OPTIONS] FILE

需要额外安装

进入主界面后  
Ctrl+X  Exit 退出  
Ctrl+O  保存文件  
Ctrl+Y  上一页  
Ctrl+V  下一页  
Ctrl+K  Cut 删除一行  
Ctrl+W  搜索
Ctrl+\  搜索替换

**find 查找文件**

```shell
# 在 / 目录查找文件名包含 pcre 的结果
find / -name pcre

# 在当前目录查找文件名包含 mysql 的内容，缩小范围，减少查找时间
find ./ -name mysql

# 指定要搜索的类型是文件(file)
find ./ -type f

# 指定要搜索的类型是目录(directory)
find ./ -type d

# 按指定的时间戳查找文件
# 访问时间戳 (atime)：最后一次读取文件的时间。
# 修改时间戳 (mtime)：文件内容最后一次被修改的时间。
# 更改时间戳 (ctime)：上次更改文件元数据的时间（如，所有权、位置、文件类型和权限设置）
find ./ -type f -atime +365
# 这里例子是查找最后一次读取文件的时间超过1年的文件，加号表示大于，减号表示小于，假如要查找正正那一天，就不加任何符号。

# -size 按大小查找文件
# -size  expression，使用加号和减号代表大于/小于，支持b(默认，512字节块)、c(字节)、w(双字节)、k、M、G单位
find . -type f -size +10M -size -1G
# 这里例子是查找文件大小在 10M到1G之间的

# 在找到文件后执行命令
find . -type f -atime +365 -exec rm -rf {} \;
# 使用 -exec 参数，{} 代表用于查找结果的占位符
```

**格式化JSON文本**

cat json.txt | python -m json.tool > json.json
curl url | python -m json.tool

**硬链接**

ln  文件路径  存放软链接的目录

**软链接（快捷方式）**

ln -s  目标文件路径  软链接文件的路径

硬链接和软链接的区别

软链接可以针对目录，硬链接不可以
软链接可以跨越分区，硬链接不可以
源文件被删除后，软链接不可用，硬链接可以继续用

对硬链接的理解，其实就是在源文件被删除后，文件系统会确保所有硬链接正常使用，原理是文件节点会被保留，仅仅是某一个路径的索引被删除。

### 4.2 文本操作

**输出文本内容**
cat  file

**输出文本内容开头N(默认10)行**
head file
-n  number

**输出文本最尾N(默认10)行内容**
tail file
-n  number
-f   跟随（follow）输出，有新内容追加时输出到控制台

**分屏输出文本内容**
more  file
缺点：只能往后翻，不能往前翻
Enter 键往下翻一行
Space 空格键往下翻一页

**文本查看**
less  file
特点：可以往上翻也可以往下翻
操作用法
Enter 键往下翻一行
Space 空格键往下翻一页
PageUp  往上翻一页
向上键  往上翻一行
/text   除号+内容，搜索指定的内容
按箭头上下进行翻页
按q退出
如果出现乱码，则要设置变量，再重新执行less命令
export LESSCHARSET=latin1

**输出内容到文件，覆盖原有内容**
内容 > 文件
ls -al > ./files.tree

还可以对一些正在被其他进程使用的日志文件，进行清空（使用单引号表示的空白字符串覆盖文件）
echo '' > /var/log/something/xxx.log

**输出到文件，追加内容**
内容 >> 文件

**排序**

sort
-n  --number-sort  排序的字段作为数值进行排序，那么 11 就会比2 大，默认是按字符串ASCII码大小进行排序，2就会比11 大。
-k  --key=keyindex  指定要排序的字段列的序号，从1开始
-t  --field-separator  指定列的分隔符
-r  --reverse  降序排序，默认为升序
-u  --unique  去除重复的行

**筛选内容**

grep  content
如果 content 包含空格，则用双引号包含字符串
-C number  输出上下文（context）的行数
-v  content  反向筛选，筛选出不包括 content 的项目
--color=auto  自动高亮关键词的颜色

例子
ps -aux | grep 80   // 筛选绑定80端口的进程
ps -aux | grep tcping | grep -v grep   筛选出 tcping 进程并排查 grep 行

还可以使用变量来设置默认使用的参数：
export *GREP_OPTIONS*='--*color=auto*'
这个代表 grep 命令默认都高亮显示筛选的内容

**统计字数、行数**

wc
记忆关键词：word count

-l  统计行数
-w  统计字数，一个字被定义为由空白、跳格或换行字符分隔的字符串。

**编码转换**

iconv -f encoding -t encoding inputfile -o outputfile
-f  from 从什么编码
-t  to  转成什么编码
-o  output 输出到文件

**vi 文本编辑器**

命令模式：

i  进入编辑模式，Esc 退出编辑模式
^  游标去到该行第一个字符
$  游标去到该行最后一个字符
Ctrl+F  下一页
Ctrl+B  上一页
dd  删除整行
(vim) u  撤销
(vim) Ctrl+R  恢复撤销
:q!  退出但不保存
:wq  写入并退出
/字符串  在游标后面查找字符串
?字符串  在游标前面查找字符串
:set nu  显示行号
:set nonu  不显示行号
:set fileencoding  查看文档编码

### 4.3 查看硬件信息

**查看 CPU 信息**

记忆关键词：PROC  cpu
cat /proc/cpuinfo
cat /proc/cpuinfo | grep "model name" | awk -F": " '{print $2}'
lscpu

**查看CPU型号**
cat /proc/cpuinfo | grep name | cut -f2 -d: | uniq -c

**查看物理CPU个数**
cat /proc/cpuinfo | grep "physical id" | sort | uniq | wc -l

**查看每个物理CPU中Core的个数**
cat /proc/cpuinfo | grep "cpu cores" | uniq

**查看逻辑CPU的个数**
cat /proc/cpuinfo | grep "processor" | wc -l

**查看内存容量**

记忆关键词：PROC  memory

cat /proc/meminfo | grep MemTotal

**查看硬盘信息**
fdisk -l
fdisk -l | grep -E "^Disk /dev/" | grep -v "/dev/mapper/" | grep -v "/ dev/loop" | grep -v "/dev/ram"

**查看某个硬盘的详细信息**
smartctl --all /dev/sda
Vendor         品牌名，如果不是正常名字则可能是虚拟硬盘
User Capacity  容量

**查看硬盘是SSD还是HDD**
cat /sys/block/<disk_name>/queue/rotational
rotational 是旋转值，1代表机械，0代表固态

### 4.4 查看系统信息

**查看操作系统的名称和版本**
cat /etc/os-release
假如 /etc/os-release 不存在，则使用
cat /etc/*-release
结果可能同时包含 Oracle Linux, CentOS Linux, Red Hat 等，其中 Oracle Linux 和 CentOS Linux 都是 Red Hat 的分支

**Red Hat 家族发行版查看系统版本**
记忆关键词：editable config redhat
cat /etc/redhat-release

**查看内核版本**
uname -a

**修改主机名**
vi /etc/sysconfig/network  # 修改其中HOSTNAME的属性，修改后不会立即生效，重启后才生效
hostname newHostname  # 不想重启就用这个，这个会即时生效，但重启后会加载 /etc/sysconfig/network 的配置，两种结合

**查看主机名**
/bin/hostname
hostnamectl

**设置主机名**
hostnamectl set-hostname servername.example.org

**查看系统产品名称，可以判断当前机子是物理机还是虚拟机**
dmidecode -s system-product-name
有的系统不支持这个命令

lshw -class system

输出为网页格式
lshw -html >info.html

**检测虚拟化信息，可以判断当前机子是物理机还是虚拟机**
systemd-detect-virt

**系统运行时间**
/usr/bin/uptime |awk '{print $3,$4}'

**查看模块信息**
lsmod

**查看系统安装时间**
stat / | awk '/Birth: /{print $2 " " $3}'
利用根目录的文件创建时间

**打印环境变量**
printenv

### 4.5 查看系统运行情况

**查看 CPU 使用率**

vmstat | awk '{if(NR==3)print $13+$14}'

vmstat 输出结果取第3行的第 13 列 user time + 14 列 system time

**查看系统资源整体使用情况**
top
顶部包含了进程信息、处理器信息、内存使用信息

**%Cpu(s)  代表 CPU 每秒的百分比**
us  user time，用户进程占用率，包括 nice time
sy  system time 内核空间占用率，包括 hard IRQ time 和 Soft IRQ time
ni  nice time 具有优先级的用户进程占用率 
id  idle time 空闲 CPU 比率
wa  waiting time 等待输入输出的CPU时间百分比
hi  硬中断（Hardware IRQ time）占用率
si  软中断（Soft IRQ time）占用率
st  steal time，被强制等待（involuntary wait）虚拟 CPU 的时间，此时 Hypervisor 在为另一个虚拟处理器服务。

功能键
1  显示每一个逻辑 CPU 的使用率
大写M  按内存占用率倒序排序
大写P   按CPU使用率大小排序
T  根据时间/累计时间进行排序
c  切换显示命令名称和完整命令行
m 切换显示内存信息。
q  退出程序

**Mib Mem  内存信息**
total  物理内存总量
used   已使用的物理内存总量
free   空闲内存总量
buff/cache  用作内核缓存的内存量
KiB Swap  交换区
total  交换区总量
used   已使用的交换区总量
free   空闲交换区总量
avail Mem  可用于进程下一次分配的物理内存数量
中间是进程信息
按 q 退出

**查看内存使用情况**
free -h
-h  human-readable，易读的，以 M 或 G 为单位显示

buff  (Buffer Cache) 是一种 I/0 缓存，用于内存和硬盘的缓冲，是 io 设备的读写缓冲区根据磁盘的读写设计的，把分散的写操作集中进行，减少磁盘碎片和硬盘的反复寻道，从而提高系统性能。
cache (Page Cache) 是一种高速缓存，用于 CPU 和内存之间的缓冲,是文件系统的 cache。把读取过的数据保存起来，重新读取时若命中(找到需要的数据)就不要去读硬盘了，若没有命中就读硬盘。其中的教据会根据读取频率进行组织，把最频繁读取的内容放在最容易找到的位置，把不再读的内容不断往后排，直至从中删除。

buff 和 cache 都是占用内存，两者都是 RAM 中的数据。简单来说，buff 是即将要被写入磁盘的，而 cache 是被从磁盘中读出来的。

还可以使用的内存量=free+(buff/cache)

**简易查看内存总容量（单位：G）**
free 命令使用 MB 作为单位输出，取第2行的第 2 列 total 数据。

free -m |awk '{if(NR==2)printf "%.1f",$2/1024}'

**简易查看内存已使用空间（单位：G）**
free 命令使用 MB 作为单位输出，取第2行的第 2 列 total - 最后1列的 available。

free -m |awk '{if(NR==2) printf "%.1f",($2-$NF)/1024}'

**简易查看内存空闲空间（单位：G）**
free 命令使用 MB 作为单位输出，取第2行的最后1列的 available。

free -m |awk '{if(NR==2) printf "%.1f",$NF/1024}'

**查看虚拟内存情况**

vmstat

**查看所有进程**

**查找某个进程（按程序名）**

ps -aux | grep zabbix

**输出详细信息**

ps -lf

字段：
PID  进程ID
PPID  父进程ID
S  state 状态
PRI  优先级
NI  nice 值

进程状态
R 是 RUNNING，包括已在CPU上执行中的和已排队并就绪的
S 是睡眠状态的 INTERRUPTIBLE 可中断状态，代表进程在等待某一条件
D 是睡眠状态的 UNINTERRUPTIBLE 不可中断的
K 是睡眠状态的 KILLABLE
T 是已停止
Z 是僵停状态的 ZOMBLE 状态，除进程本身之外的所有资源都已释放
X 是僵停状态的 DEAD 状态

**列出排名前10的内存占用进程**

ps aux | sort -rk 4,4 | head -n 10

**查找进程ID**

pidof  process_name
假如有多个 pid，则用空格分隔

pgrep process_name

**查看进程树**

pstree

**结束进程**

kill pid
pid 是端口号，数值型

pkill process_name
根据进程名发送信号

**控制进程**

kill  信号编号or短名称

| 信号编号     | 短名称 | 含义     | 用途                                                 |
| ------------ | ------ | -------- | ---------------------------------------------------- |
| 1            | HUP    | 挂起     |                                                      |
| 2            | INT    | 键盘中断 | 可以使用 Ctrl+C 发送                                 |
| 3            | QUIT   | 键盘退出 |                                                      |
| 9            | KILL   | 中断     | 立即终止程序，无法被拦截                             |
| 15 (Default) | TERM   | 终止     | 终止程序，可以被拦截、忽略或处理，终止程序的友好方式 |
| 18           | CONT   | 继续     | 恢复一个已停止的进程                                 |
| 19           | STOP   | 停止     | 暂停，无法被拦截或处理                               |
| 20           | TSTP   | 键盘停止 |                                                      |

如：kill -9 pid

**通过 pid 文件结束进程**

cat /tmp/zabbix_agentd.pid | xargs kill

**批量结束进程**

ps -ef | grep test | grep -v grep | awk '{print $2}' | xargs kill -9
ps -ef | grep test | grep -v grep | cut -c 9-15 | xargs kill -9

其中 test 是需要查找的程序名

$2 是因为 ps 返回结果第二列是进程ID

**查看系统I/O情况**

iotop 一个类似 top 的工具，实时输出各进程I/O信息
iotop -Po

-o, --only                      只显示正在产生I/O的进程或线程，运行过程中，可以通过按o随时切换
-d SEC, --delay=SEC   设置显示的间隔秒数，支持非整数
-p PID, --pid=PID        只显示指定进程（PID）的信息
-u USER, --user=USER   显示指定用户的进程信息
-P, --processes           只显示进程，不显示所有线程
-a, --accumulated      累积的I/O，显示从iotop启动后每个进程累积的I/O总数，便于诊断问题
-k, --kilobytes             显示使用KB单位，默认是根据数值用B、Kb、Mb等显示

控制按键：iotop 运行过程中对功能进行控制

左/右箭头按键：改变排序的字段
r：reverse，改变排序顺序
o：只显示有IO输出的进程
p：进程/线程的显示方式的切换
a：显示累积使用量
q：退出

**查看正在打开的文件**

lsof  (list opened files)
该程序列出系统中已经打开的文件，包括普通文件，目录，块特殊文件，字符特殊文件，正在执行的文本引用，库，流或网络文件（例如：网络套接字，NFS文件或UNIX域套接字）。

-i<条件>  列出符合条件的进程。（4、6、协议、:端口、 @ip ）
-p pid  输出指定进程打开的文件
-u userName  输出指定用户打开的文件
-c string  输出 COMMAND 列中包含 string 的项

| 列字段   | 含义                             |
| -------- | -------------------------------- |
| COMMAND  | 命令名称                         |
| PID      | 进程ID                           |
| TID      | 线程ID，如果为空代表列出的是进程 |
| TASKCMD  | 任务名称，通常与 COMMAND 相同    |
| USER     | 用户ID号或登录名                 |
| FD       | 文件描述符                       |
| TYPE     | 与文件关联结点的类型             |
| DEVICE   | 设备号                           |
| SIZE/OFF | 文件大小/偏移量，以字节为单位    |
| NODE     | 文件结点                         |
| NAME     | 文件挂载点和文件所在的系统       |

通过端口查进程ID
lsof -i:10050
通过 PID 查程序文件路径
lsof -p 1234

**查看硬盘/存储分区使用情况**

记忆关键词：display file system

df -h | sort
-h  human-readable，易读的，以 M 或 G 为单位显示
-T  file system type，输出文件系统类型

**查看目录文件空间占用情况**

记忆关键词：disk usage

查看当前目录各子节点硬盘占用情况
du -lh --max-depth=1

**查看占用空间最大的文件**

du -ah ./ | sort -rh | head -n 10

du 的 a 参数代表 all，展示所有文件和文件夹，h 参数代表 humanable，以可读性强的方式展示文件大小
sort 的 r 参数代表 reverse，反转排序，h 参数代表 humanable，以可读性强的方式进行排序
head 的 n 参数代表输出结果的行数

**查看系统是否安装有图形界面（Redhat 系列）**

rpm -qa | grep -i xorg-x11

### 4.6 时区和语言设置

**查看时区**
timedatectl
Time zone 一行

**更改时区**

timedatectl set-timezone ‘Asia/Shanghai’

或者

sudo rm -f /etc/localtime
sudo ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

或者

tzselect 修改时区

**禁用 NTP （时间同步服务）**
timedatectl set-ntp false

**启用 NTP （时间同步服务）**
timedatectl set-ntp true

**配置 NTP （时间同步服务）**
RHEL 6 代以前配置文件：/etc/ntp.conf
RHEL 7 代以后配置文件：/etc/chrony.conf
pool 参数：NTP 服务器地址，支持多个，一行一个
重启 NTP 服务并设置开机自动启动
systemctl restart chronyd
systemctl enable chronyd.service

**查看 NTP 服务器**
chronyc sources -v

**强制同步系统时间**
chronyc -a makestep

**查看当前系统语言**
locale

**查看系统支持的语言**
locale -a

**查看系统是否支持某个语言**
locale -a | grep zh_CN

**安装某个语言**
yun install glibc-langpack-zh.x86_64

**设置系统默认语言**
echo LANG=zh_CN.UTF-8 > /etc/locale.conf

**查看终端使用的语言**
echo $LANG

**修改终端使用的语言(临时修改，当前终端生效)**
export LANG="zh_CN.UTF-8"

### 4.7 日期和时间

**输出年-月-日**
echo $(date +%F)

**输出时:分**
echo $(date +%R)

**输出年月日**
echo $(date +%Y%m%d)

**以月历的方式显示日期**

cal

### 4.8 系统设置

**查看系统启动模式**
systemctl get-default
返回值：
graphical.target 代表多用户图形界面
multi-user.target 代表多用户命令行界面

**设置系统默认启动模式**
systemctl set-default graphical.target
systemctl set-default multi-user.target

**查看系统参数**
/sbin/sysctl -a
/sbin/sysctl -a | grep param-name
-a -all  all variables

**编辑系统参数**
/etc/sysctl.conf

**重新加载系统参数**
/sbin/sysctl -p

**资源限制配置**
/etc/security/limits.conf

**环境变量**

| 配置文件                                | 影响的范围                   | 有效期   | 备注                                                         |
| --------------------------------------- | ---------------------------- | -------- | ------------------------------------------------------------ |
| /etc/profile                            | 全局用户，应用于所有的 Shell | 永久有效 |                                                              |
| $HOME/.bash_profile<br />$HOME/.profile | 当前用户，应用于所有的 Shell | 永久有效 | 生效方法：关闭当前终端窗口，重新打开一个新终端窗口<br />或输入 “source .bash_profile” 命令 |
| /etc/bash_bashrc                        | 全局用户，应用于所有的 Shell | 永久有效 | 生效方法：系统重启                                           |
| ~/.bashrc                               | 局部当前，应用于当前的 Shell | 临时有效 |                                                              |
|                                         |                              |          |                                                              |

```Shell
# 推荐用第二种（$HOME/.bash_profile），避免因编辑错误，造成 bash/vi 无法使用的故障。
cd $HOME
nano .bash_profile
# 最后面插入
export PATH=/usr/local/mysql/bin:PATH
source .bash_profile
```

**自动启动**

在 /etc/init.d 目录下创建脚本文件（可使用软链接），/etc/init.d 是指向 /etc/rc.d/init.d 的软链接，两个都是同一个东西

软链接到可执行程序
ln -s /usr/loca/sbin/zabbix_agentd /etc/init.d/zabbix_agentd

软链接到 startup.sh 之类的脚本
ln -s /opt/apache-tomcat/bin/startup.sh /etc/init.d/tomcat

**虚拟内存设置**

```shell
# 创建一个8GB大小的swap文件
sudo fallocate -l 8G /swapfile
# 设置文件权限
sudo chmod 600 /swapfile
# 创建swap格式
sudo mkswap /swapfile
# 启用swap文件
sudo swapon /swapfile
# 查看内存状况，是否已经生效
free -h

# 修改 /etc/fstab 文件，加入以下内容，使永久生效：
/swapfile swap swap defaults 0 0
# 检查 mount 配置是否正确
sudo mount -a
```

### 4.9 用户管理

**用户信息记录文件**
/etc/passwd

**格式**

```
UserName:x:UID:GID:Note:HomeFolder:Shell
```

**用户密码记录文件**
/etc/shadow

**修改密码**

passwd
修改当前用户的密码

passwd username
修改指定用户的密码

**查看所有用户命令**
groups

**创建用户**
adduser username
该命令会在 /etc/passwd 添加一行记录，并创建用户目录
-d home_dir 设定使用者的家目录为 home_dir ，预设值为预设的 home 后面加上使用者帐号 loginid

**创建用户**
useradd username
该命令不创建用户目录

假如不想让用户进行登录，则设置 -s 参数到 nologin
useradd -M -s /sbin/nologin nginx

**删除用户**
userdel -r loginid
-r  移除用户的home目录

**以管理员身份运行**
sudo 命令

sudoer 配置文件，指定允许以管理员身份运行的用户。
/etc/sudoers

root    ALL=(ALL)    ALL
wen    ALL=(ALL)    ALL

**以指定用户运行**

su -c command user_name
command 假如带有参数，则使用双引号包含起来
user_name 是运行的用户

**切换用户**

su user
只切换用户，不改变环境变量

su - user
切换用户，并更新用户的环境变量 (推荐这种方式)

su -
切换 root 用户并更新用户的环境变量的简写

**创建用户组**

groupadd  group_name

该命令会在 /etc/group 文件中添加一个新的组条目

**查看用户组信息**

cat /etc/group

**添加用户到组里**

usermod -aG newgroup username

-a  append，插入一个用户到组



### 4.10 运行程序

方法一：可执行文件或批处理文件，具有执行权限 (x)，可直接执行
/bin/bash
./startup.sh

方法二：作为参数执行，此处的 *.sh 不需要执行权限
/bin/bash ./check.sh

**后台运行**

nohup command [params] &

nohup command [params] | $! > xx.pid
运行程序并获取 pid

### 4.11 磁盘管理

使用 LVM (Logical Volume Manager 逻辑卷管理)

物理卷 (PV) 就是物理的磁盘分区，磁盘分区是不能跨硬盘的

卷组 (VG) 就是把一个或多个物理卷 (PV) 合并为一个逻辑上的扩展分区，里面使用到的物理分区不需要连续，可以跨硬盘

逻辑卷 (LV) 就是扩展分区里面再细分的分区，由于卷组是可以跨硬盘的，所以逻辑卷 (LV) 也是可以跨硬盘的，这种分区使用时不会用死一个硬盘，而且扩展容量方便

```shell
# 第一步：创建物理分区作为逻辑卷管理
# 查看存在的硬盘和分区情况
fdisk -l
# 查看分区挂载情况
df -h
# 使用 fdisk 创建新分区
fdisk /dev/vdb
# 列出分区情况，确保有未使用空间
Command (m for help): p
# 其中 Disklabel type 是分区表类型，下面命令是创建新的 GPT 分区表
Command (m for help): g
# 查看支持的分区类型，其中找到 Linux LVM
Command (m for help): l
# 创建新分区，全部参数默认，就是使用最大连续空余空间
Command (m for help): n
# 更改分区类型
Command (m for help): t
# 然后按 L，列出所有分区类型，找到 Linux LVM 的代号后，按 q 返回
Partition type or alias (type L to list all): L
# 其中找到 Linux LVM 的序号为 30，输入相应数字
Partition type or alias (type L to list all): 30
# 写入分区表并退出
Command (m for help): w
# 重新查看存在的硬盘和分区情况
fdisk -l

# 第二步：创建物理卷 (PV)
pvcreate /dev/vdb1 [/dev/vdb2...]
# 查看当前物理卷信息 pvs pvscan pvdisplay
pvs
# 删除物理卷，用 pvremove
# pvremove /dev/vdb1 [/dev/vdb2...]

# 第三步：创建卷组 (VG)
vgcreate vg_name pv1 [pv2...]
# 查看卷组 vgs vgscan vgdisplay
vgs
# 删除卷组，用 vgremove
# vgremove vg_name

# 第四步：创建逻辑卷 (LV)
# lvcreate -L <lv_size> <vg_name> -n <lv_name>
# lvcreate -l 100%free <vg_name> -n <lv_name>
lvcreate -l 100%free vg_opt -n lv_opt
# 查看逻辑卷 lvs lvscan lvdisplay
# 删除逻辑卷，使用 lvremove
# lvremove /dev/vg_opt/lv_opt

# 第五步：格式化逻辑卷分区
mkfs.xfs /dev/vg_opt/lv_opt
# lv 文件放在 /dev/mapper 里
ls /dev/mapper/
# 修改系统挂载配置
nano /etc/fstab
# 在最后面添加：
/dev/mapper/vg_opt-lv_opt   /opt   xfs   defaults 0 0
# 挂载分区
mount -a
```



### 4.12 查询编码

```shell
# ASCII 码
man ascii
```



## 五、网络功能

**查看网卡(interface)设置**  
ifconfig device_name
记忆关键词：interface config
其中 lo 是 local (本地网络)

**Redhat 8 查看 IP 地址**
ip addr show  devName

**Redhat/CentOS 8 查看连接**
nmcli connection show
其中 nmcli 是 NetWork Manager 服务的 Client 客户端程序，Redhat 8 后的产物
这个命令只输出在连接状态的物理网络适配器和虚拟网络适配器，不包括 lo
输出字段：

| 字段名 | 含义                                                  |
| ------ | ----------------------------------------------------- |
| NAME   | 设备名称，如 ens160、virbr0                           |
| UUID   | 32位字符串，输出格式采用标准的 UUID 格式 (8-4-4-4-12) |
| TYPE   | 设备类型，如 ethernet、bridge                         |
| DEVICE | 设备名称，和 NAME 一样                                |

**Redhat/CentOS 8 查看网络适配器状态**
nmcli device status
输出结果包括所有网络适配器和 lo

| 字段名     | 含义                                            |
| ---------- | ----------------------------------------------- |
| DEVICE     | 设备名称，如 ens160、virbr0                     |
| TYPE       | 设备类型，如 ethernet、bridge、loopback、tun    |
| STATE      | 设备状态，如 connected、disconnected、unmanaged |
| CONNECTION | 连接名称                                        |

**Redhat/CentOS 8 激活一个连接**
nmcli device connect devName

**修改网卡设置**

网卡配置文件在：/etc/sysconfig/network-scripts/

记忆关键词：editable config system network

配置文件参数

| 参数名                | 说明                                                         |
| --------------------- | ------------------------------------------------------------ |
| DEVICE                | 网卡名称                                                     |
| ONBOOT=yes            | 系统启动时启用                                               |
| BOOTPROTO             | IP 类型，可取值：static, dhcp, none(相当于 static)           |
| IPADDR=10.10.101.102  | IP 地址                                                      |
| NETMASK=255.255.255.0 | 网络掩码                                                     |
| PREFIX=24             | 网络段长度，填数字，如C类地址为 24，网络掩码和前缀选择其一即可 |
| GATEWAY=10.10.101.254 | 默认网关                                                     |
| DNS1                  | 域名解析服务器地址                                           |

设置多IP，在属性后面添加数字即可
IPADDR0
NETMASK0
GATEWAY0

**Redhat/CentOS 8 修改网络连接配置**

```Shell
# 修改 IP 和手动设置
nmcli connection modify  ens224  ipv4.addresses  192.168.146.128/24
nmcli connection modify  ens224  ipv4.method manual
nmcli connection modify  ens224  ipv4.dns 223.5.5.5
nmcli connection modify  ens224  ipv4.gateway 192.168.146.1
nmcli connection reload ens224  # 重新加载网卡配置
nmcli connection down ens224  # 停用网卡
nmcli connection up ens224  # 启用网卡
ip addr show ens224  # 查看最新配置确认生效
route -n   # 查看网关是否已在路由表上，并且在 ens224 网卡上
```

modify 也可以一行过：
nmcli connection modify  ens224  ipv4.addresses  192.168.146.128/24  ipv4.method manual  ipv4.dns 223.5.5.5  ipv4.gateway 192.168.146.1

**Redhat/CentOS 8 手动添加连接对设备进行关联**
nmcli connection add type ethernet ifname ens61 con-name ens161
ifname  参数后面跟的是接口名
con-name  参数后面跟的是连接名，一般与接口名相同

**Ubuntu 禁用网卡**
ifdown devicename

**Ubuntu 启用网卡**
ifup devicename

**Redhat/CentOS 8 重启网卡**
nmcli c reload
nmcli c up eth0

**设置 DNS 域名解析**
配置文件：/etc/resolv.conf
配置格式：nameserver  ip

**ping (测试外部主机的连通性)**  
ping HOST
HOST 可以是域名或 IP  
按 Ctrl+C 停止

**发送一个 HTTP 请求并打印内容**
curl url

-k, -insecure  忽略证书校验

**发送一个 POST 请求**
curl -X POST --header "Content-Type:multipart/form-data" --data "" url
curl -X POST --header "Content-Type:application/json" --data "{}" url

**上传文件**
curl -X POST --header "Content-Type:multipart/form-data"  -F "file=@/path/to/file.txt" -F "name=file" url

**下载文件**
wget URL
curl URL > filename.ext

**跨主机复制文件**
scp local_file remote_username@remote_ip:remote_file
scp remote_username@remote_ip:remote_file local_file
scp -rC local_folder remote_username@remote_ip:remote_folder
scp -rC remote_username@remote_ip:remote_folder local_folder

-r  递归复制目录
-C  使用压缩
-v  显示详细信息

**后台执行方法：**
按 Ctrl + z 暂停任务
jobs 查看当前的任务
STOPPED 代表已暂停
RUNNING 代表正在运行
bg %1       // 将进程号为1的进程恢复到后台运行
重新执行 jobs 确定原有任务在运行中
ps -aux | grep scp

**排查网卡硬件故障**
ethtool [option...] devname

需要额外安装

官方文档：
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s1-ethtool

**查看路由状况**

输出路由表
route -n
-n  参数表示不解析名字，列出速度会快一点

路由表字段：

| 字段名      | 含义                                                         |
| ----------- | ------------------------------------------------------------ |
| Destination | 目标 IP 网段                                                 |
| 网关        | 网关 IP                                                      |
| Genmask     | 网络掩码                                                     |
| Flags       | 标记，U 代表 up，指当前路由是有效的运行的，G 代表 gateway，表示此网关为一路由器，H 代表 host，表示此网关为一主机，R 代表 reinstate route，使用动态路由重新初始化的路由，D 代表 dynamically，表示此路由是动态性地写入，M 代表 modified，表示此路由是由路由守护程序或导向器动态修改，! 表示此路由当前为关闭状态 |
| Metric      | 优先级，值越小越优先                                         |
| Ref         |                                                              |

**解析域名**
在 bind-utils 包里
nslookup HOST [DNS_HOST]
如果不指定 DNS_HOST，则使用当前系统所使用的 DNS 服务器

dig HOST

**路由追踪**
traceroute host

**检测端口能否正常连接**
telnet host port

**根据端口查看进程**
netstat -anp | grep 80

**查看所有监听端口并去重**
netstat -tln | awk ‘NR>2{sub(“.*:”,“”,$4);print $4}’ | sort | uniq

**SSH 客户端下载文件**
sz file
需要安装 lrzsz

**SSH 客户端上传文件**
rz file
需要安装 lrzsz

**查看 TCP 连接数**
netstat -ant | grep -i tcp | wc -l

**查看 HTTP 80 端口 连接数**
netstat -ant | grep 80 | wc -l

**网络监控**

iptraf
注记：IP transfer

-i iface   在指定网络接口 iface 上开启IP流量监视，iface 为 all 则监视所有的网络接口
-d iface  在指定网络接口 iface 上开始监视明细的网络流量信息，iface 指相应的 interface

**查看网络适配器的速率**

iftop

**发送邮件**

需要安装第三方软件 mailx
yum install -y mailx

编辑配置文件 /etc/mail.rc，以 outlook 邮箱为例
set from="example@outlook.com"
set smtp="smtp.office365.com"
set smtp-auth-user="example@outlook.com"
set smtp-auth-password="password"
set smtp-auth=login
set smtp-use-starttls
set ssl-verify=ignore
set nss-config-dir=/etc/pki/nssdb/

用 mailx 命令发送一个测试邮件

```shell
echo 'hello, world' | mailx -v -s "test" example@outlook.com 
```

用 mail 命令发送一个测试邮件

```shell
echo 'hello, world' | mail -v -s "test" example@outlook.com
```



## 六、高级功能

### 6.1 压缩包操作

**创建一个 tar 压缩包文件**
tar -czv -f test1.tar.gz /home/www
-c  create  创建压缩包
-z  使用 gzip 算法压缩，扩展名用 gz
-v  verbose  输出详细信息（涉及到的文件名）
-f   file 指定文件名

**解压 tar 压缩包文件**
tar -xzv -f test1.tar.gz
-x  extract  解压

**查看 tar 压缩包的文件**
tar -tv -f /home/bk0406-1717.tar.gz
-t  list files

### 6.2 dnf  RHEL8 / CentOS8 的 RPM 软件包管理器

**安装软件**
dnf install package_name

**重新安装软件**
dnf reinstall package_name

**升级指定软件**
dnf update package_name

**升级所有软件**
dnf update

**只下载某个包但不安装**
dnf download name

**检查系统软件包的更新**
dnf check-update

**删除软件包**
dnf remove package_name
dnf erase package_name

**删除无用孤立的软件包**
dnf autoremove

**删除缓存的无用软件包**
dnf clean all

**刷新缓存**
dnf makecache

**列出所有已安装的RPM包**
dnf list installed

**搜索软件库中的RPM包**
dnf search package_name

**查看软件包详情**
dnf info package_name

### 6.3 yum RedHat / CentOS 旧版本的 RPM 软件包管理器

**搜索软件包**
yum search name

**查看软件包信息** 
yum info name

**安装软件包**
yum install name
-y 一键确认，中途不再询问

**只下载某个包但不安装**
yum install name --downloadonly --downloaddir=/root/Download

**卸载软件包**
yum remove name

**更新已安装的软件**
yum update

### 6.4 rpm RedHat / CentOS 软件包安装器

**查询(query)某个包是否已安装**
rpm -q package_name

**查询所有 rpm 包**
rpm -a

**安装某个包**
rpm -i package_name

**删除某个包**
rpm -e package_name

### 6.5 apt Ubuntu 的 DEB 软件包管理器

**安装 apt 软件包**
apt-get install package_name

### 6.6 查看程序的动态链接库依赖

ldd  (list dynamic library dependency)
ldd  program

### 6.7 文档及多媒体操作

**转换MP3文件的ID3编码** 
mid3iconv -e GBK *.mp3

**Word 转 Pdf**
soffice --headless --invisible --convert-to pdf <源office文档路径> --outdir <目录存储目录>

### 6.8 ACME 网站 SSL 免费证书生成

**安装 ACME**
curl https://get.acme.sh | sh

**用 ACME 生成证书**
sudo ./acme.sh --issue -d breezes.name -d www.breezes.name -w /data/www/breezes/
-d 参数：域名
-w 参数：网站目录



### 6.9 修改 jar 包里面的文本文件

```shell
vim x.jar
# 找到对应的文本文件，编辑后，保存退出即可
```



### 6.10 ear 包解压

```shell
# 列出 ear 程序包里的内容
jar -tvf your-application.ear
# 解压 ear 程序包
jar -xf your-application.ear
```

### 6.11 ODBC

```shell
# 查询系统是否已安装 ODBC (基于 RPM 的系统)
rpm -qz unixODBC;
# 查询系统是否已安装 ODBC (基于 Debian 的系统)
apt search unixODBC;

# 安装 ODBC (基于 RPM 的系统)
sudo yum install unixODBC unixODBC-devel;
# 安装 ODBC (基于 Debian 的系统)
sudo apt-get update;
sudo apt-get install unixodbc unixodbc-dev;

```

**odbcinst.ini‌ 配置文件**

这个文件包含了ODBC驱动程序的安装信息。通常，您不需要手动编辑这个文件，除非您要安装一个不在标准软件包中的 ODBC 驱动程序。‌

**odbc.ini‌ 配置文件**

这个文件定义了数据源的名称 (DSN) 以及连接到该数据源所需的信息。例如，配置一个名为 MyDSN 的 MySQL 数据源可能看起来像这样：

```
[MyDSN]
Driver = MySQL
Description = MySQL connection to mydb
Server = localhost
Port = 3306
Database = mydb
User = myuser
Password = mypassword
```

**测试 ODBC 连接**

```shell
isql MyDSN myuser mypassword
```

如果连接成功，您应该会看到数据库服务器版本的信息和一些命令提示符，表明您已经成功连接到了数据库。

如果您在测试过程中遇到任何问题，可能需要检查 ODBC 驱动程序的安装情况、配置文件中的参数设置，或者数据库服务器的状态。

### 6.12 系统优化

**释放内存**
echo 3 > /proc/sys/vm/drop_caches
drop_caches的值可以是0-3之间的数字，代表不同的含义：<br>0：不释放（系统默认值）<br>1：释放页缓存<br>2：释放dentries和inodes<br>3：释放所有缓存



## 七、服务功能

Linux 里面，常住后台的都叫服务，不一定是提供网络功能的才叫服务。

### 7.1 服务管理

服务的程序一般放在 /etc/init.d 里，如：
/etc/init.d/nginx
有些精简版系统既没有 service 也没有 systemctl 程序，可以从这个地方启动服务

**启动服务**
service serviceName start
systemctl start service_name

**重启服务**
service serviceName restart
systemctl reload service_name

**停止服务**
service serviceName stop
systemctl stop service_name

**查看服务状态**
service serviceName status

**设置服务开机启动**
systemctl enable serviceName

**设置服务开机不启动**
systemctl disable serviceName

**新增服务**
chkconfig --add name



**网络服务重启**

RHEL 6 以前
/etc/init.d/network restart
service network restart

RHEL 7
/etc/init.d/network restart
systemctl restart network.service

RHEL 8 后废弃了 network 服务，改用 NetworkManager 服务，该服务不能被关闭
控制网络使用 nmcli (network manager client)
nmcli connection reload [interface]
nmcli connection down [interface]
nmcli connection up [interface]



### 7.2 一次定时任务 at

该服务由 atd 支持

**at 命令使用语法**
at  [options] [time_expression]

**选项：**
-l  输出已有的任务，额外的 atq 命令相当于这个选项
-d  额外的 atrm 命令相当于这个选项
-f  从指定文件读入任务而不是从标准输入输出

**时间表达式支持：**
HH:MM YYYY-MM-DD
HH:MM[am|pm] [Month] [Date]
HH:MM[am|pm] + number [minutes|hours|days|weeks]

**添加任务**

```shell
at 16:00
/home/tom/test.sh
<EOT>
```

EOT 就是按 Ctrl+D 结束

**删除任务 atrm**

```shell
# atrm job_number
atq
1    day month  date HH:MM:SS YEAR a root
atrm 1
```



### 7.3 定时任务 cron

该词来源于希腊语 chronos(χρνο)，原意是时间。

定时任务由 crond 服务支持

**系统级配置文件（更改后需要手动更新）**
/etc/crontab
含义：cron table
注意：执行用户需要 root 或属于 sudoer
格式：{minute} {hour} {day-of-month} {month} {day-of-week} {full-path-to-shell-script}

crontab 使用的环境变量与shell不同，可以显式定义，比如：
JAVA_HOME=/opt/jdk/
PATH=$PATH:$JAVA_HOME/bin

**从配置文件加载定时任务**
crontab /etc/crontab

**列出当前用户的定时任务**
crontab -l

**编辑当前用户的定时任务（退出后立刻生效）**
crontab -e

**重启服务**
service crond restart
含义：cron deamon  守护进程

**定时任务日志**
/var/log/cron
或 mail 邮件查看 /var/spool/mail/username



### 7.4 firewalld 防火墙

CentOS 7 以上版本自带的防火墙

**查看防火墙状态**
firewall-cmd --state

**更新防火墙规则**
firewall-cmd --reload

**查看所有允许的服务**
firewall-cmd --list-services

**查看所有打开的端口**
firewall-cmd --list-ports

**获取所有支持的服务**
firewall-cmd --get-services

**永久添加允许的服务**
firewall-cmd --add-service ssh --permanent
firewall-cmd --add-service http --permanent
firewall-cmd --add-service https --permanent

**永久添加允许的端口**
firewall-cmd --add-port=$PORT/tcp --permanent



### 7.5 iptables 防火墙

CentOS 6 以下版本自带的防火墙

**启动服务**
/etc/init.d/iptables start

**保存规则到配置文件**
service iptables save



### 7.6 UFW 防火墙

Ubuntu 自带的防火墙

**启用防火墙**
ufw enable

**禁用防火墙**
ufw disable

**允许 ssh 端口通过(支持端口名称和端口号)**
ufw allow ssh
ufw allow 21

**允许 ftp 端口通过(支持端口名称和端口号)**
ufw allow ftp
ufw allow 25

**允许 www 端口通过(支持端口名称和端口号)**
ufw allow www
ufw allow 80



### 7.7 SE-Linux

配置文件：/etc/selinux/config

**获取当前模式**
getenforce

**临时关闭**
setenforce 0

**开启**
setenforce 1

**永久关闭，重启生效**
vim /etc/sysconfig/selinux
SELINUX=disabled

**查看所有规则**
getsebool -a

**设置规则**
setsebool

-P  代表永久修改

**设置 FTP**
setsebool -P allow_ftpd_full_access=1
setsebool -P ftp_home_dir=1



### 7.8 SSH 服务

**安装：**
CentOS: yum install -y sshd
Ubuntu: apt-get install ssh

**配置：vi /etc/ssh/sshd_config**
PermitRootLogin no            // 不允许 Root 用户登录
PermitEmptyPasswords no        // 不允许空密码登录
AllowUsers kevin            // 只允许指定用户登录

**服务程序：/etc/init.d/ssh**

**启动服务**
service sshd start

**设置开机启动**
systemctl enable sshd

**免密码证书登录配置**

```shell
# 在服务器上生成公私密钥对
ssh-keygen -t dsa
# id_dsa.pub 是公钥，id_dsa 是私钥，公钥放服务端，私钥放客户端
# 输出公钥到用户目录下 .ssh 目录的 authorized_keys 文件
cat id_dsa.pub >> /home/username/.ssh/authorized_keys
```

putty 配置：私钥文件 id_dsa 拷贝到 Windows 客户端上，由于 putty 用到证书格式不一样，需要转换一下
打开 puttygen 工具，load 该私钥文件，然后 save private key，比如 id_dsa_for_putty
打开 putty，新建或修改原有的 Session，选择 Connection -> SSH -> Auth，在 Private key file for authentication 选择 id_dsa_for_putty，选择 Connection -> Data，Auto-login username 填写用户名，登录即可。



### 7.9 FTP服务

新版本 SSH 服务已自带 ftp 服务，建议直接开启

安装：
Ubuntu  #apt-get install vsftpd
CentOS  #yum install -y vsftpd

配置文件：
Ubuntu  /etc/vsftpd.conf
CentOS  /etc/vsftpd/*

服务程序：
/etc/init.d/vsftpd

根目录：
Ubuntu  /srv/ftp
CentOS  /var/ftp

默认禁止根目录的写权限，如果出错则清除根目录的写入权限
sudo chmod a-w ftp

匿名用户访问配置
anonymous_enable=YES  查找并去掉注释

匿名用户上传权限
anon_upload_enable=YES  查找并去掉注释

匿名用户创建目录的权限
anon_mkdir_write_enable=YES 查找并去掉注释

匿名用户权限掩码
anon_umask=066  手动添加(建议先查找一下)，默认为066



### 7.10  文件共享 Samba

**安装**

```she
sudo apt-getinstall samba
sudo yum install -y samba
# 设置开机自启动
systemctl enable smb
chkconfig smb on
```

**配置**

```she
# 以 /var/netdisk 为例
# 创建网盘目录
sudo mkdir /var/netdisk
# 修改配置文件，添加网盘目录
sudo nano /etc/samba/smb.conf

[netdisk]
comment =/var/netdisk
path = /media/Mydisk
writeable = yes
browseable = yes
create mask = 0755
directory mask = 0755

# 创建用户，并输入密码
pdbedit -a username
# 查看 samba 的用户
pdbedit -L

# 重启服务
sudo samba restart
```

**Windows 客户端配置**

```
# 打开我的电脑，在地址栏输入：
\\server_ip
```

假如无法访问服务，则打开控制面板-程序与功能-启动或关闭windows功能-启动SMB服务

### 7.11 Nginx

二进制包编译

```shell
yum -y install pcre-devel zlib-devel gcc gcc-c++ make

useradd -M -s /sbin/nologin nginx

./configure \
--prefix=/usr/local/nginx \                      #指定nginx的安装路径
--user=nginx \                                        #指定用户名
--group=nginx \                                        #指定组名
--with-http_stub_status_module                        #启用 http_stub_status_module 模块以支持状态统计

make && make install

ln -s /usr/local/nginx/sbin/nginx /usr/local/sbin/        #让系统识别nginx的操作命令
```

**检查配置文件**
nginx -t

Nginx 由一个主进程管理多个子进程，子进程提供真正的服务，当子进程崩溃时，主进程会重新创建一个新的。重新加载配置、重启服务，需要由主进程去发射信号，而不需要把所有进程都结束然后重新开启。

**发送一个重新加载配置的信号**
nginx -s reload

**发送一个重新开启服务进程的信号**
nginx -s reopen

**发送一个停止服务的信号**
nginx -s stop

**发送一个退出程序的信号**
nginx -s quit

**常见问题**

页面出现 404 not found
本地路径缺少文件或 index.html
代理的URL路径缺少文件或 index.html
本地路径缺少读取权限

页面出现 403 premission denied
本地路径缺少读取权限
代理的URL缺少读取权限

### 7.12 Tomcat (Java Web Server)

**启动**
bin/startup.sh

**关闭**
bin/shutdown.sh  

**服务配置**
conf/server.xml 文件

**应用内容配置**
conf/context.xml 文件

**运行日志**
logs/catalina.out 文件

**Windows 系统安装 Tomcat 服务**
%TOMCAT_HOME%/bin/service.bat install

**Windows 系统移除 Tomcat 服务**
%TOMCAT_HOME%/bin/service.bat remove

### 7.13 PHP

**启动 PHP-FPM 服务**
service php-fpm start

**查看某个 PHP 模块是否已开启**
php -m|grep ldap



### 7.14 Redis

**关闭/开启/重启服务(通过apt-get或者yum install安装的)**
/etc/init.d/redis-server stop
/etc/init.d/redis-server start
/etc/init.d/redis-server restart

**关闭/开启/重启服务(通过源代码安装的)**
./redis-cli -h 127.0.0.1 -p 6379 shutdown
./redis-cli -h 127.0.0.1 -p 6379 -a password shutdown
如果关闭不成功，则使用 kill -9 杀死 redis 进程，-9 代表强制结束进行，不接受拦截或取消
cd /path/to/redis/src
./redis-server
无密码方式启动
./redis-server ../redis.conf
有密码方式启动



### 7.15 Tuned 性能调优

**安装**
dnf install tuned -y

**查看支持的配置列表**
tuned-adm list
最后一行 Current active profile 显示当前所使用的配置

**查看激活的配置**
tuned-adm active

**查看建议的配置**
tuned-adm recommend

**设置新的配置**
tuned-adm profile  profile_name



## 八、脚本

**以 root 身份运行整个 shell 脚本**

#!/usr/bin/sudo /bin/bash



## 九、Web 接口

**查天气**
curl wttr.in/Foshan?lang=zh
Foshan 可以替换为其他城市名





## 附录一：常见错误

**安装软件时出现 Protected multilib versions**

原因：存在多个不同版本的依赖包造成

解决：yum 安装的话，尝试添加 --setopt=protected_multilib=false 参数

**无法读取 so 文件**

```
# 在 ld.so.conf.d 添加一个配置
nano /etc/ld.so.conf.d/oracle.conf
# 添加内容到配置文件里面：
/oracle/client/

# 执行 ldconfig 刷新配置
ldconfig

# 重新执行程序

# 如果仍然不行，用 ldd 检查一下程序的动态链接库
ldd sqlplus

# 留意一下 not match 的 so 项目在目录里是否存在

```

**so 文件不匹配当前程序版本**

示例：The “libpq.so” loaded mismatch the version of gsql, please check it.

此问题是由于环境中使用的 x.so 的版本与当前程序的版本不匹配导致的，请通过“ldd <程序名>”命令确认当前加载的x.so的版本，并通过修改 LD_LIBRARY_PATH 环境变量来加载正确的 x.so。

**yum 安装软件时出现依赖版本不匹配问题**

原因：依赖里面部分在本地已安装，但版本不适用于需要安装的新软件

解决：删除旧有的依赖，然后重新安装新软件

```shell
yum remove 旧有的包  # 比如 mysql-libs
yum install 新的软件包   # 比如 mysql
```

**火狐浏览器缺少视频解码器**

```shell
# 查看 H264 解码器
gst-inspect-1.0 | grep h264parse
sudo apt-get install gstreamer1.0-plugins-bad
```

