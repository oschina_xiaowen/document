# MySQL 笔记



**导出数据表数据或结构**

```shell
mysqldump -h<host_name> -u<user_name> -p database_name [table_name] > output.sql
```

-v  verbose 啰嗦的，显示详细信息

-P  port_number  指定自定义端口



**查询表结构**

SELECT 
f.TABLE_NAME 表名,
f.TABLE_COMMENT 表注释,
t.COLUMN_NAME 字段名称,
t.COLUMN_TYPE 数据类型,
t.COLUMN_KEY 约束1PRI主键约束2UNI唯一约束2MUI可以重复索引,
t.IS_NULLABLE 是否为空,
t.COLUMN_COMMENT 字段注释,
t.COLLATION_NAME 使用字符集,
t.DATA_TYPE 字段类型,
t.EXTRA 额外配置
FROM(
	SELECT
	sch.TABLE_SCHEMA,
	sch.TABLE_NAME,
  sch.TABLE_COMMENT
	FROM information_schema.`TABLES` sch
	WHERE TABLE_SCHEMA='test'
  -- 加上可过滤到表
  -- AND TABLE_NAME='hostss'
) f
LEFT JOIN information_schema.COLUMNS t ON f.TABLE_SCHEMA=t.TABLE_SCHEMA AND f.TABLE_NAME=t.TABLE_NAME;



**查询数据库所有表的记录数量**

```sql
SELECT table_name, TABLE_ROWS
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'your_database_name'
order by table_name;
```



**集群查分片IP（各节点的物理IP）**

```shell
# shell 执行
mysql -h IP [-P 3306] -u root -p -c
mysql> /*proxy*/ show status;
```



**函数**

FROM_UNIXTIME  时间戳格式转为字符串

```sql
SELECT FROM_UNIXTIME(1587211731, '%Y-%m-%d %H:%i:%s');
SELECT FROM_UNIXTIME(now(), '%Y-%m-%d %H:%i:%s');
SELECT FROM_UNIXTIME(now(), '%Y-%m-%d');
```

DATE_FORMAT  日期型转为字符串

```sql
SELECT DATE_FORMAT(now(), '%Y-%m-%d %H:%i:%s');
```

UNIX_TIMESTAMP  字符串转时间戳

```sql
SELECT UNIX_TIMESTAMP('2020-04-18 20:35:31');
```
