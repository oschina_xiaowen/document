# NFS 安装配置 #

## 安装 ##

**通过 yum 安装**
yum -y install nfs-utils rpcbind

**设置开机启动**
systemctl enable nfs-server
systemctl enable rpcbind

**启动服务**
service nfs-server start
service rpcbind start

## 创建共享目录 ##

mkdir /var/nfs

允许写入，则设置 777 权限
chmod 777 /var/nfs

不允许写入，则设置 755 权限
chmod 755 /var/nfs

## 配置 NFS ##

**NFS 目录配置文件**
位置：/etc/exports
添加目录
/var/nfs 192.168.1.0/24(rw,sync,no_root_squash,no_all_squash)

这里是针对 192.168.1.0/24 网段可以访问，也可以单独设置某一个IP
rw：可读写的权限
ro：只读的权限
anonuid：可以自行设定一个 User id，这个UID必需要存在于你的/etc/passwd当中
anongid：可以自行设定这个 Group id
sync：资料同步写入到内存与硬盘当中
async：资料会先暂存于内存当中，而非直接写入硬盘
insecure：允许从这台机器过来的非授权访问
no_root_squash：登入NFS主机，使用该共享目录时相当于该目录的拥有者，如果是root的话，那么对于这个共享的目录来说，他就具有root的权限，这个参数『极不安全』，不建议使用
root_squash：登入NFS主机，使用该共享目录时相当于该目录的拥有者。但是如果是以root身份使用这个共享目录的时候，那么这个使用者（root）的权限将被压缩成为匿名使用者，即通常他的UID与GID都会变成nobody那个身份
all_squash：不论登入NFS的使用者身份为何，他的身份都会被压缩成为匿名使用者，通常也就是nobody

**重新加载NFS配置**
exportfs -r

## 配置防火墙

CentOS 8 以上
firewall-cmd --add-service nfs --permanent
firewall-cmd --reload

## 查看本机 NFS 目录 ##

showmount -e localhost

## Windows 10 客户端配置 ##

设置 -> 应用 -> 程序和功能 -> 启用或关闭 Windows 功能：勾选上 NFS 服务

**挂载 NFS 目录**
mount -t nfs 192.168.1.13:/var/nfs X:

其中 192.168.1.13 是 NFS 服务器的 IP 地址

**卸载挂载**
umount X:

