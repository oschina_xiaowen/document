# Nginx 编译安装



## 安装依赖包

```shell
yum -y install pcre-devel zlib-devel gcc gcc-c++ make
```



## 开启 SSL 的话，还需要安装以下依赖库

```shell
yum install -y zlib zlib-devel openssl openssl-devel
```



## 创建运行用户和组

```shell
useradd -M -s /sbin/nologin nginx
```



## 解压并进入目录，配置、安装

```shell
cd nginx-1.12.0/
./configure \
--prefix=/usr/local/nginx \                   #指定nginx的安装路径
--user=nginx \                                #指定用户名
--group=nginx \                               #指定组名
--with-http_stub_status_module  \              #启用 http_stub_status_module 模块以支持状态统计
--with-http_ssl_module                               #启用 SSL 模块

nginx -s stop    # 假如是重装/升级，先关掉 nginx

make && make install

ln -s /usr/local/nginx/sbin/nginx /usr/local/sbin/        # 让系统识别nginx的操作命令
```

假如出错提示找不到 openssl，则添加 --with-openssl=/usr/local/ssl 参数

假如出错提示找不到 .openssl/include 之类的，则修改 auto/lib/openssl/conf，注销掉4行，新增4行

或参考：http://www.manongjc.com/detail/24-gqyjjrymxwidzzj.html

```shell
#CORE_INCS="$CORE_INCS $OPENSSL/.openssl/include"
#CORE_DEPS="$CORE_DEPS $OPENSSL/.openssl/include/openssl/ssl.h"
#CORE_LIBS="$CORE_LIBS $OPENSSL/.openssl/lib/libssl.a"
#CORE_LIBS="$CORE_LIBS $OPENSSL/.openssl/lib/libcrypto.a"

CORE_INCS="$CORE_INCS $OPENSSL/include"
CORE_DEPS="$CORE_DEPS $OPENSSL/include/openssl/ssl.h"
CORE_LIBS="$CORE_LIBS $OPENSSL/lib/libssl.a"
CORE_LIBS="$CORE_LIBS $OPENSSL/lib/libcrypto.a"
```



## 生成证书

```shell
openssl genrsa -des3 -out server.key 2048
# 会有两次要求输入密码
# 假如不想要密码，则再运行下面的
openssl rsa -in server.key -out server.key
# 服务器证书的申请文件
openssl req -new -key server.key -out server.csr
# 创建 CA 证书
openssl req -new -x509 -key server.key -out ca.crt -days 3650
# 创建server.crt
openssl x509 -req -days 3650 -in server.csr -CA ca.crt -CAkey server.key -CAcreateserial -out server.crt
```

最后产出4个文件

| 文件       | 作用                                                         |
| ---------- | ------------------------------------------------------------ |
| server.key | 服务器证书（私钥）                                           |
| server.csr | 服务器证书请求文件，csr 是Certificate Signing Request 的缩写 |
| ca.crt     | CA 证书                                                      |
| server.crt | 服务器证书（公钥），crt 是 certificate 的缩写                |

其中 server.key 和 server.csr 就是 nginx SSL 配置需要的

