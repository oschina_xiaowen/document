# Oracle DB 笔记

## 一、安装配置

### 1.1 服务端安装配置

**oracle 用户添加 ORACLE_HOME 环境变量**

```shell
su - oracle
cd $HOME
./.bash_profile
# 结尾添加：
export ORACLE_HOME=/opt/oracle/product/19c/dbhome_1
export PATH=$PATH:/opt/oracle/product/19c/dbhome_1/bin
export ORACLE_SID=oracle19c
```

### 1.2 客户端安装配置

```shell
# 编辑 $home/.bash_profile
# 添加2个环境变量
ORACLE_HOME=/path/to/oracle/sqlplus_file_path
LD_LIBRARY_PATH=/path/to/oracle/so_file_path
# 刷新环境变量
source $home/.bash_profile

# 尝试执行 sqlplus
# 如果出现 so 文件找不到，尝试刷新 LD 配置（需要管理员权限）
ldconfig

```



## 二、SQL 写法

### 2.1 分组序号

ROW_NUMBER() OVER(partition by 分组字段 ORDER BY 排序字段)

```sql
--例：
ROW_NUMBER() OVER(partition by DEPTNO ORDER BY SAL)
--这个是按每个部门计算薪酬序号
```

### 2.2 Database Link (DB link)

```SQL
select * from table_name@dblink_name;
```



## 三、函数

**sys_guid()**
返回一个唯一的ID



## 四、数据表

### 4.1 表空间

**查询表空间**

```sql
select * from dba_tableSpaces;
-- 需要有 DBA 权限的用户执行
```

**查询表空间使用情况**

```sql
select TABLESPACE_NAME,
       SUM(T.BYTES)/1024/1024/1024     USED_G,
       SUM(T.MAXBYTES)/1024/1024/1024  TOTAL_G,
       ROUND(T.BYTES/T.MAXBYTES*100, 4)||'%'  RATE_G
FROM TABLESPACE_NAME='WBJH_DAT'
GROUP BY TABLESPACE_NAME
;
```

**查询表空间数据文件**

```sql
select * from v$datafile;
```

**查询表空间对应的数据文件**

```sql
select s.tablespace_name, f.file_name, s.*, f.*
from dba_tableSpaces s, dba_data_files f
where s.tablespace_name=f.tablespace_name
  and s.tablespace_name='TS_GSZM_YW_DATA';
-- 需要有 DBA 权限的用户执行
```

**查询表空间文件**

```sql
select T.TABLESPACE_NAME, T.*
FROM DBA_DATA_FILES T
--WHERE TABLESPACE_NAME=''
ORDER BY T.TABLESPACE_NAME, file_name;
```

**创建表空间**

```sql
create tablespace user_data
　　--logging
　　datafile 'D:\oracle\oradata\Oracle9i\user_data.dbf'
　　size 4g
　　--autoextend on next 1g maxsize 30g
　　--extent management local;
```

**删除表空间**

```sql
drop tablespace tab_name [including contents] [cascade constraints]
```

including contents 表示在删除表空间的时候把表空间中的数据文件一并删除。

cascade constraints 示在删除表空间的时候把表空间的完整性也一并删除。比如表的外键，和触发器等就是表的完整性约束。

**表空间文件调整容量**

```sql
alter database datafile 'file_name' resize 20G;
alter database datafile 'file_name' resize 20G autoextend on next 2g maxsize 30g;
```

**表空间追加文件**

```sql
alter tablespace tab_name add datafile 'file_name' size 20G;
alter tablespace tab_name add datafile 'file_name' size 20G autoextend on next 2g maxsize 30g;
```

**查询临时表空间文件**

```sql
select * from dba_temp_files;
```

**查询临时表空间**

```sql
select tableSpace_Name,
       round(sum(user_bytes) / 1024 / 1024 / 1024, 2) user_gb,
       round(sum(maxBytes) / 1024 / 1024 / 1024, 2) max_gb,
       round(sum(user_bytes) / sum(maxBytes) * 100, 2) used_rate
from dba_temp_files
group by tableSpace_Name;
```

**创建临时表空间**

```sql
create temporary tablespace TS_CXBB_TMP2
tempfile '/u01/oracle/app/oradata/DBPRIMARY/TS_CXBB_TMP2.dbf'
size 200m;
```

**创建临时表空间**

```sql
create temporary tablespace TS_CXBB_TMP2
tempfile '/u01/oracle/app/oradata/DBPRIMARY/TS_CXBB_TMP2.dbf'
size 200m;
```

### 4.2 数据表占用空间

```sql
-- 查当前用户
SELECT
    SEGMENT_NAME AS table_name,
    SEGMENT_TYPE,
    BYTES / 1024 / 1024 AS size_mb
FROM
    USER_SEGMENTS
WHERE
    SEGMENT_TYPE = 'TABLE';

-- 查所有表
SELECT segment_name "表名",
    segment_type "对象类型",
    sum(bytes) / 1024 / 1024 "占用空间（MB）"
 FROM dba_extents
 WHERE 1=1
  -- AND segment_name = '表名'
 GROUP BY segment_name, segment_type
 ORDER BY "占用空间（MB）" DESC;
```

### 4.3 约束

**查询表字段约束**

```sql
select * from dba_constraints  where table_name='name';
select * from dba_cons_columns where table_name='name';
```

**禁用表字段约束**

```sql
alter table table_name disable constraint constraint_name;
```



### 4.4 整理数据字典，查多个表的数据量

```sql
# 创建表
create table table_dictionary (
    xh   number   primary key,
    name varchar2(100) not null,
    comments   varchar2(200),
    row_count   number
);
# 导入数据
# 更新数据
update table_dictionary dic
set row_count = (select num_rows from user_tables t where t.table_name=dic.name);

```



## 五、ASM 磁盘组

### 5.1 查 ASM 磁盘组使用情况

```sql
SELECT INST_ID, NAME,
       FREE_MB/1024  FREE_GB, TOTAL_MB/1024  TOTAL_GB,
       (TOTAL_MB - FREE_MB) / 1024  USED_GB,
       (TOTAL_MB - FREE_MB) / TOTAL_MB * 100  USED_RATE
FROM GV$ASM_DISKGROUP WHERE INST_ID=2;
```



## 六、同义词

### 6.1 创建同义词

```sql
create synonym [current_user.]synonym_name
for [other_user.]object_name
-- 一般情况下 synonym_name 和 object_name 相同
```



## 七、锁记录

### 7.1 查询被锁的对象

```SQL
select s.sid, s.status, s.serial#, wait_time_micro, wait_time_micro/1000/1000/60  wait_time_minute,
       s.username, s.schemaname, s.osuser, s.process, s.machine, s.terminal, s.logon_time,
       l.type, a.sql_text
from v$session s, v$lock l, v$sqlarea a
where s.sid=l.sid and s.prev_sql_addr=a.address
  and s.username is not null
  --and s.status = 'ACTIVE' AND s.state='WAITING'
order by s.sid;
-- s.sid 和 s.serial# 可用于结束会话
```

### 7.2 解除被锁的对象

```sql
select * from DBA_objects where object_name = 'object_name';
-- 获取 object_id
select 'ALTER SYSTEM KILL SESSION ''' || SID || ',' || SERIAL# || ''';' FROM v$session
where SID IN (SELECT SID FROM V$ENQUEUE_LOCK T WHERE T.TYPE='TO' AND ID1='object_id')
-- 查询结果是一个命令语句，复制到命令窗口执行。
```



## 八、会话

### 6.1 结束会话

```sql
ALTER SYSTEM KILL SESSION 'sid, serial#' IMMEDIATE;
```



## 七、查系统参数

**查看 processes 和 sessions 参数**
show parameter processes
show parameter sessions



## 八、安全管理

### 8.1 监控用户权限设置

赋予 connect 角色权限

如果该用户要查 DBA 的视图，则要执行
grant select any dictionary to user_name;



## 九、SQL*Plus

命令不区分大小写

**oracle 用户执行**
sqlplus "/as sysdba"

**sqlplus 程序里**
startup 启动数据库实例
shutdown 关闭数据库实例
quit 或 exit 退出 SQL*Plus

select * from v$log;

**查询数据库块大小**

```sql
-- SQL 执行
select value from v$parameter where name='db_block_size';
```

或者命令窗口执行：

show parameter db_block_size;



## 十、存储过程

**定义变量**

```sql
# 在 is 段内
is
err_msg   varchar2(1000); -- 定义字符串
err_code  number;         -- 定义数值
```

**系统自带变量**

sql%rowcount   sql 影响的行数

**捕获错误**

```sql
exception
  when others then
    err_msg := sqlcode || sqlerrm;  -- sqlcode 是出错执行的SQL语句，sqlerrm 是错误信息
    
```



## 十一、索引

### 11.1 索引分析查询

```sql
-- 命令行模式
analyze index INDEX_NAME validate structure;
-- SQL 模式
select height, DEL_LF_ROWS/LF_ROWS from index_stats;
```





## 三、Oracle 数据字典

### 3.1 常用系统表

| 表名                  | 含义                    | 内容                                   |
| --------------------- | ----------------------- | -------------------------------------- |
| all_tab_comments      | all table comments      | 存放各个表的名称、类型、拥有者和注释   |
| all_col_comments      | all column comments     | 存放表的各个字段的名称、所属表名、注释 |
| dba_tab_modifications | DBA table modifications | 存放表的修改记录                       |
| dba_tablespaces       | DBA table spaces        | 存放表空间信息                         |
| dba_users             | DBA users               | 存放数据库用户                         |
| DBA_DATA_FILES        | DBA data files          | 数据文件                               |
| user_objects          | User Objects            | 用户对象                               |
| all_synonyms          |                         | 同义词记录                             |

### 3.2 常用系统视图

| 视图名      | 含义 | 内容                                                        |
| ----------- | ---- | ----------------------------------------------------------- |
| v$version   |      | 版本信息                                                    |
| v$parameter |      | 参数（processes 等）                                        |
| ALL_VIEWS   |      | 视图信息（VIEW_TYPE 值为 'MATERIALIZED VIEW' 代表物化视图） |

### 3.3 查看 Oracle 版本

```SQL
SELECT * FROM v$version;
SELECT version FROM v$instance;
SELECT * FROM PRODUCT_COMPONENT_VERSION;
```

### 3.4 数据表记录

#### 3.4.1 列出所有表

```sql
select * from all_tab_comments;
```

#### 3.4.2 列出某个用户里的所有表

```sql
select * from all_tab_comments where owner='WBJH';
```

#### **3.4.3 列出所有用户表和注释**

```sql
SELECT a.TABLE_NAME, b.COMMENTS
FROM user_tables a, user_tab_comments b
WHERE a.TABLE_NAME=b.TABLE_NAME
ORDER BY TABLE_NAME;
```

#### 3.4.4 搜索某个表在哪个用户

```sql
select * from all_tab_comments where TABLE_NAME like '%DI_HY%';
```

#### 3.4.5 关键词搜索某个表（基于注释）

```sql
select * from all_tab_comments where COMMENTS like '%申报%';
```

#### 3.4.6 关键词搜索某个表的某个字段

```sql
select * from all_col_comments where table_name='SB_FCJY' and comments like '%日期%';
```

#### 3.4.7 查询某个表的更新记录

```sql
select * from dba_tab_modifications where table_name like 'HBJH%';
```

#### 3.4.8 查询数据库用户

```sql
select * from dba_users;
```



### 3.5 数据表结构查询

```sql
SELECT  t1.Table_Name     AS "表名称",
        t3.comments       AS "表说明",
        t1.Column_Name    AS "字段名称",
        t1.DATA_TYPE      AS "数据类型",
        t1.DATA_LENGTH    AS "长度",
        t1.NullAble       AS "是否为空",
        t2.Comments       AS "字段说明"
FROM cols t1
LEFT JOIN user_col_comments t2
	 ON t1.Table_name = t2.Table_name
	AND t1.Column_Name = t2.Column_Name
LEFT JOIN user_tab_comments t3
	 ON t1.Table_name = t3.Table_name
where T3.OWNER='WBJH'
ORDER BY t1.Table_Name, t1.Column_ID;
```



### 3.6 查数据表的创建时间和最后分析时间

```sql
select t.table_name, t.last_analyzed, o.craeted from user_tables t, user_objects o
where o.object_name=t.table_name and t.table_name in ();
```





## 十三、常见问题

### **(1) ORA-14450:试图访问已经在使用的事务处理临时表**

主要是临时表的 session 还在被占用

```sql
-- 找到对象的 object_id
SELECT * FROM DBA_OBJECTS WHERE OBJECT_NAME='TEMP_TB';  -- (假定object_id为12345)
-- 通过 object_id 获取所有 session，并直接生成 kill 语句：
SELECT 'ALTER SYSTEM KILL SESSION '''||SID||','||SERIAL#||''';' FROM V$SESSION WHERE SID IN(SELECT SID FROM V$ENQUEUE_LOCK T WHERE T.TYPE='TO' AND ID1='12345');
-- 生成了命令，打开命令窗口执行
alter system kill session '*******'; -- 执行语句类似这种
```

### **(2) 密码过期，重置密码提示不符合密码复杂度**

解决方法：可以设新密码就设置新密码，不能就先修改密码复杂度配置，修改密码，然后再回复密码复杂度配置

```sql
# 查看用户的 profile，获取 profile 名称
select username, profile from dba_users where username = 'WBJH_GSQZ';
# 用 profile 查 PASSWORD_LIFE_TIME 的设置，复制原有设置，等下要还原
select * from dba_profiles where profile='DEFAULT' and resource_name='PASSWORD_LIFE_TIME';
# 修改密码验证策略
alter profile default limit PASSWORD_VERIFY_FUNCTION null;
# 修改密码
alter user WBJH_GSQZ IDENTIFIED BY "password";
# 还原原来的密码验证策略
alter profile default limit PASSWORD_VERIFY_FUNCTION VERIFY_FUNCTION;
```

