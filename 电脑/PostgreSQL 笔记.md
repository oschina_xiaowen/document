# PostgreSQL 笔记



## 编译安装

**依赖包**

perl-ExtUtils-Embed  readline-devel  zlib-devel  pam-devel  libxml2-devel  libxslt-devel  openldap-devel  python-devel  gcc-c++  openssl-devel  cmake

**安装步骤**

```shell
# 下载并解压
get ...
tar -zxvf postgresql-xx.xx.tar.gz
cd postgresql-xx.xx

# 查看用户是否存在
id postgres
# 添加用户组及用户
groupadd postgres
useradd -g postgres  postgres
# 再次查看可以查看对应的uid gid 
id postgres

# 创建数据目录并授权
mkdir -p /data/postgresql/data
chown -R postgres:postgres /data/postgresql/data

# 编译并安装
./configure --prefix=/usr/local/postgresql
make
make install
chown -R postgres:postgres /usr/local/postgresql

# 配置 postgres 用户的环境变量
# 切换到 postgres 用户
su - postgres
# 编辑 postgres 用户环境变量
vim .bash_profile
# 添加如下内容
export PGHOME=/data/postgresql
export PGDATA=/data/postgresql/data
PATH=$PATH:$HOME/bin:$PGHOME/bin
# 保存退出
# 使环境变量生效
source .bash_profile

# 切换回 root 用户，修改环境变量
nano .bash_profile
# 找到 PATH 那一行，在后面追加 /usr/local/postgresql/bin
# 或者新建一行，填 PATH=$PATH:/usr/local/postgresql/bin
# 使环境变量生效
source .bash_profile

# 尝试执行 psql 看是否能用
psql --help
```



## 客户端使用

**连接服务器**
psql -h IP [-p 端口] -U 用户名 -d 数据库名
-h  database_hostname
-p  port  default: 5432

**常用命令说明**

```postgreSQL
\? #所有命令帮助
\l #list, 列出所有数据库清单
\d #display 列出数据库中所有表
\dt #display table 列出数据库中所有表
\d [table_name] #显示指定表的结构
\di #display index 列出数据库中所有 index
\dv #display view 列出数据库中所有 view
\h #help, sql命令帮助
\q #quit 退出连接
\c [database_name] #切换到指定的数据库
\c #显示当前数据库名称和用户
\conninfo #显示客户端的连接信息
\du #display user  列出所有用户
\dn #显示数据库中的schema
\encoding #显示字符集
select version(); #显示版本信息
\i testdb.sql #input 执行sql文件
\x #扩展展示结果信息，相当于MySQL的\G
\o /tmp/test.txt #ouput 将下一条sql执行结果导入文件中
```



## 用户管理

**创建用户**
create user 用户名 password '密码';

**设置用户只读权限**
alter user 用户名 set default_transaction_read_only = on;

**设置用户可操作的数据库**
grant all on database 数据库名 to 用户名;

**授权用户可操作的模式和权限**
grant select on all tables in schema public to 用户名;

GRANT ALL ON TABLE public.user TO mydata;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE public.user TO mydata_dml;
GRANT SELECT ON TABLE public.user TO mydata_qry;

**撤回用户在 public 模式下的权限（删除用户前需要做）**
revoke select on all tables in schema public from 用户名;

**撤回在information_schema模式下的权限（删除用户前需要做）**
revoke select on all tables in schema information_schema from 用户名;

**撤回在pg_catalog模式下的权限（删除用户前需要做）**
revoke select on all tables in schema pg_catalog from 用户名;

**撤回对数据库的操作权限（删除用户前需要做）**
revoke all on database 数据库名 from 用户名;

**删除用户**
drop user 用户名;



## 数据库管理

**删除数据库（删除前先关闭连接）**
drop database 数据库名;

**查数据库的编码**

```sql
select pg_encoding_to_char(encoding)
from pg_database
where datname='database_name'
```

**查锁表的客户端信息**

```sql
SELECT
    relname AS table_name,
    l.locktype,
    pg_blocking_pids(l.pid) AS blocking_processes,
    l.mode,
    l.granted,
    c.client_addr,
    c.client_port,
    c.backend_start,
    c.query_start,
    c.state,
    c.waiting
FROM
    pg_locks l
JOIN
    pg_stat_all_tables t ON l.relation = t.relid
JOIN
    pg_stat_activity c ON l.pid = c.pid
WHERE t.relname = 'table_name'; -- 替换为你要检查的表名；
```



## 常用函数

**查看版本**
SELECT version();



## 系统表

**查数据表**

```sql
SELECT tablename FROM pg_tables
--where tableowner=''
--where tablename like '%pg_%'
;
```



