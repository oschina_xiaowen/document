# SQLite 笔记



## SQLite是什么？

SQLite 是**嵌入式**关系数据库管理系统。它是开源的，独立的，**无服务器的**，**零配置**和事务性SQL数据库引擎。

SQLite 与其他 SQL 数据库不同，SQLite **没有单独的服务器进程**。它直接读取和写入普通磁盘文件。 具有多个表，索引，触发器和视图的完整 SQL 数据库包含在**单个磁盘文件**中。

SQLite 最初是在2000年8月设计的。它被命名为SQLite，因为它与其他数据库管理系统(如SQL Server或Oracle)不同，它是**非常轻量的**(小于500Kb大小)。

更多请阅读：https://www.yiibai.com/sqlite/what-is-sqlite.html 



## SQLite 命令种类

- **DDL**：数据定义语言
- **DML**：数据操作语言
- **DQL**：数据查询语言
- **SQLite 的点命令**



## DDL 数据定义语言

- **CREATE**：此命令用于创建表，数据库中的表或其他对象的视图。
- **ALTER**：此命令用于修改现有的数据库对象，如表。
- **DROP**：此命令用于删除整个表，数据库中的表或其他对象的视图。



## DML 数据操作语言

- **INSERT**：此命令用于创建记录。
- **UPDATE**：用于修改记录。
- **DELETE**：用于删除记录。



## DQL 数据查询语言

- **SELECT**：此命令用于从一个或多个表中检索某些记录。



## 点命令

| 命令               | 功能                                                         |
| ------------------ | ------------------------------------------------------------ |
| .help              | 帮助                                                         |
| .exit              | 退出                                                         |
| .show              | 查看 SQLite 命令提示符的默认设置                             |
| .backup ?db? file  | 备份数据库(默认“`main`”)到文件中                             |
| .databases         | 附件数据库的列表名称和文件                                   |
| .dump ?table?      | 以sql文本格式转储数据库，如果指定表，则只转储表匹配像模式表。 |
| .echo on/off       | 打开或关闭 echo 命令                                         |
| .header(s) on/off  | 打开或关闭标题的显示                                         |
| .import file table | 将数据从文件导入表                                           |
| .indices ?table?   | 显示所有索引的名称。如果指定表，则只显示匹配的表的索引，如模式表。 |
| .log file/off      | 打开或关闭日志记录。文件可以是 stderr/stdout                 |
| .mode mode         | 设置输出模式                                                 |
| .read filename     | 在文件名中执行sql                                            |
| .schema ?table?    | 显示创建语句。如果指定表，则只显示与模式表匹配的表。         |
| .tables ?pattern?  | 列出匹配类似模式的表的名称                                   |



