# WebLogic 笔记



## 一、服务器管理

### 1.1 健康状况

Server 的健康状况为警告，点击进入 Server 的详情，查看日志记录配置里面的日志文件名称

文件系统转到 /weblogic/Oracle/Middleware/user_projects/domains/xx_domain/servers/server_name/logs

按文件名查看相应的日志文件



配置

server 优化参数：服务器-配置-优化

JTA：服务-JTA

## 附：常见问题

（1）管理节点启动时报错：weblogic.security.SecurityInitializationException: Authentication for user  denied

修改或创建 $DOMAIN_HOME/servers/AdminServer/security/boot.properties

修改原有密码（原有密码是密文，重新以明文方式设置新密码即可，应用启动成功会重新加密），或重新设置以下内容：

```
username:MYUSERNAME
password:MYPASSWORD
```

