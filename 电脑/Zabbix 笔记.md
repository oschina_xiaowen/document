# Zabbix 笔记



## zabbix 服务重启方法

```shell
# 提高日志级别
service zabbix_server restart
# 如果系统服务不存在，则用 kill 方法
ps aux | grep zabbix_server
# 找到 preprocessing manager 一行, kill 掉这个进程
kill pid
```



## zabbix server 命令

```shell
# 提高日志级别
zabbix_server --runtime-control log_level_increase
# 降低日志级别
zabbix_server --runtime-control log_level_decrease
```



## zabbix agent 运行方法

```shell
cd /path/to/zabbix/agent/bin
zabbix_agentd -c ../conf/zabbix_agentd.conf
```



## zabbix agent 退出方法

```shell
ps -aux | grep "zabbix_agent -c"
# 获取到带c参数的进程的ID
kill {pid}
```



## 使用扩展参数

打开 zabbix_agent.conf
搜索 UnsafeUserParameters，开启用户参数
UnsafeUserParameters=1

文件最后面添加用户参数：

UserParameter 格式：参数表达式,执行命令 [$1...]
参数表达式以 xxx.xxx[*] 格式，中括号包含一个星号代表参数通配符，非必填
执行命令的 $1... 为参数，对应监控项配置的实际参数


UserParameter=ext.net.port.test[*],node D:\Software\zabbix_agent\ext_parameter\test_net_port_delay_time.js $1 $2



## 中文显示

下载中文字体（最好是 TTF 格式），放到Zabbix的网站的 fonts 目录下，如：/data/www/zabbix/fonts/

修改配置：/data/www/zabbix/include/defines.inc.php

```
//define('ZBX_FONT_NAME', 'DejaVuSans'); 
define('ZBX_FONT_NAME', 'kaiti'); 

//define('ZBX_GRAPH_FONT_NAME', 'DejaVuSans'); // font file name 
define('ZBX_GRAPH_FONT_NAME', 'kaiti');        // font file name 
```

重新刷新页面即可



## 脚本执行超时解决办法

服务端修改 zabbix_server.conf，设置 timeout 参数

客户端修改 zabbix_agent.conf，设置 timeout 参数

服务端测试能否顺利获取数据

zabbix_get --host {host} -k key_express

其中 {host} 是客户端的 hostname/ip，key_express 是监控项的表达式

