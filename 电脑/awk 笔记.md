# awk 笔记

## 主要用途

1. 能够将给定的文本内容，按照我们期望的格式输出显示，打印成报表；
2. 分析处理系统日志，快速地分析挖掘我们关心的数据，并生成统计信息；
3. 通过各种工具的组合，快速地汇总分析系统的运行信息，让你对系统的运行了如指掌；
4. 强大的脚本语言表达能力，支持循环、条件、数组等语法，助你筛选、排序、分析复杂的数据。

## 基本用法

awk 'expression' file

expression 一定要用单引号，并使用大括号包含每一段程序语句

默认是按空格和制表符划分内容

**更改分隔符的参数**

awk -F":"

## 内置变量

$0  代表每一行的记录

$n 代表第N列的内容

ARGC      命令行参数个数

ARGV      命令行参数排列

ENVIRON    支持队列中系统环境变量的使用

FILENAME    awk浏览文件名

FNR      浏览文件的记录数

FS       设置输入域分隔符，等价于命令行-F选项

NF       浏览记录的域个数

NR       已读的记录数

OFS      输出域分隔符

ORS      输出例句分隔符

RS       控制记录分隔符

## 例子

**输出每一行记录**

awk '{ print $0 }'

**输出第一列内容**

awk '{ print $1 }'

**开始时做一些操作**

比如输出表头，设定一些变量，使用 BEGIN 关键词，加大括号包含一些语句

awk 'BEGIN {print "Name      Belt\n--"} {print $1"\t"$3}' test.dat

**结尾做一些操作**

比如计数，求和，使用 END 关键词，加大括号包含一些语句

awk '{ count++ } END {print "count:", count }' test.dat

**正则表达式匹配**

在大括号前面添加“**变量 ~/exp/**”的格式

查找 Dan 这个人的信息

awk '$1 ~/Dan/ { print $0 }' ./test.dat

查找 J 开头 的人的信息

awk '$1 ~/^J/ { print $0 }' ./test.dat

也可以在操作里面使用 if

awk '{ if ($1 ~/^J/) {print $0} }' ./test.dat

**精确匹配**

在大括号前面添加“**变量 逻辑运算符 值**”的格式

awk '$3=="48" {print $0}' ./test.dat

**条件分支**

awk '{ if ($1=="M.Tansley") {print $0} }' ./test.dat

注意有两层大括号

**使用内置变量**

awk '{print NF,NR,$0} END {print FILENAME}' test.dat

**使用字符串函数**

gsub(r,s)      在整个$0中用s替代r

gsub(r,s,t)     在整个t字符串中使用s替代r

index(s,t)     返回s字符串中t字符串的第一个位置

length(s)      返回s字符串的长度

match(s,r)     测试s是否包含匹配r的字符串

split(s,a,fs)    在fs上将s分成序列a

sprint(fmt,exp)   返回经fmt格式化后的exp

sub(r,s)      用$0中r字符串第一次出现的位置

substr(s,p)     返回字符串s中从p开始的后缀部分

substr(s,p,n)    返回字符串s中从p开始长度为n的后缀部分

**格式化输出命令 printf**

类似C语言的 printf 用法

echo "65" | awk '{ printf "%c\n",$0 }'
