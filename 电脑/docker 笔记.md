# docker 笔记



## 一、基本命令

**获取镜像**
docker pull <仓库名>:<标签>

**运行镜像**，启动容器
docker run --name <容器名> -d -p 80:80 <仓库名>:<标签>
docker run -it --rm <仓库名>:<标签> bash

**列出镜像**
docker images
docker images <仓库名>

**保存新镜像**
docker commit <容器ID或容器名> [<仓库名>[:<标签>]]
--author  --message

**删除镜像 remove image**
docker rmi <镜像>
docker rmi <镜像1> [<镜像2> ...]

**启动容器（新建并启动）**
docker run -d <仓库名>:<标签>
docker run --name <容器名> -d -p 80:80 <仓库名>:<标签>

**启动已终止容器**
docker start <容器ID或容器名>

**查看容器**
docker ps
docker ps -a

**终止容器**
docker stop <容器ID或容器名>

**重启容器**
docker restart <容器ID或容器名>

**进入容器**
docker exec -it <容器名> bash

**拷贝文件到容器里**
docker cp /path/to/local/file  containerId:/path/to/destination

**查看容器改动**
docker diff <容器ID或容器名>

**导出容器**
sudo docker export <容器ID或容器名> > name.tar

**导入容器**
cat name.tar | sudo docker import - <容器名>:v1.0

**重命名容器**
docker rename oldName newName

**删除容器**
docker rm <容器ID或容器名>

**查看容器信息**
docker inspect <容器ID或容器名>

**清理所有处于终止状态的容器**
docker rm $(docker ps -a -q)

**拷贝文件到容器里**
docker cp 本地文件的路径 container_id:<docker容器内的路径>

**查看日志**
docker logs container_id



## 二、配置文件 Dockerfile

FROM <仓库名>
RUN
COPY <源路径> <目标路径>
CMD <命令>
CMD ["可执行文件", "参数1", "参数2"...]

按 Dockerfile 构建镜像
docker build -t nginx:v3 .
docker build -f ./Dockerfile -t nginx:v3 ./dist



## 三、数据卷（Volume 用法）

**创建数据容器卷**
docker volume create <容器卷名称>

**查看所有容器卷**
docker volume ls

**查看指定容器卷详情信息**
docker volume inspect <容器卷名称>

**运行镜像使用数据容器卷**
docker run -d -it --name=edc-nginx -p 8800:80 -v edc-nginx-vol:/usr/share/nginx/html nginx

**移除数据卷**
docker stop edc-nginx // 暂停容器实例
docker rm edc-nginx // 移除容器实例
docker volume rm edc-nginx-vol // 删除自定义数据卷

## 四、数据卷（Bind Mounts 用法）

**创建数据容器卷**
docker run -d -it -p 8800:80 --name=edc-nginx -v /app/wwwroot:/usr/share/nginx/html nginx



## 五、网络配置

**查看网络**
docker network ls

**创建网络**
docker network create -d bridge default-network



## 六、docker-compose 用法

**查看版本信息**
docker-compose -v

**启动服务 (要在有 yaml 文件目录下执行)**
docker-compose up -d
docker-compose up
-d 代表后台运行，默认是前台运行，可以即时查看启动日志，按 Ctrl+C 退出后会停止服务

**停止服务 (要在有 yaml 文件目录下执行)**
docker-compose down

**创建服务 (要在有 yaml 文件目录下执行)**
docker-compose create
更新了包的时候需要重新创建

**查看日志 (要在有 yaml 文件目录下执行)**
docker-compose logs
docker-compose logs -f
docker-compose logs -fn 1000 project_name

-f 代表跟住日志输出
-n 代表日志行数

**检查配置文件 (要在有 yaml 文件目录下执行)**
docker-compose config
有错误时输出错误信息，没有错误时则输出配置内容

**列出项目中所包含的镜像**
docker-compose images

**重启项目中的服务**
docker-compose restart



## 七、服务管理

启动 docker 服务

```shell
# 查看 docker 服务
ps aux | grep docker
# 启动 docker 服务
cd /usr/local/docker
nohup ./start_docker.sh & tail -f nohup.log
```

