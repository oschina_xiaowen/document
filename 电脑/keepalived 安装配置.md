# keepalived 安装配置

## 说明

OS: CentOS 8
keepalived: 2.1

## 安装

**多台机分别安装**
yum install -y keepalived

**设置开机自动启动**
systemctl enable keepalived

## 配置虚拟网卡

ifconfig 查看物理网卡名称，物理机一般为 eth0，虚拟机一般为 enp0s3
网卡配置文件在 /etc/sysconfig/network-scripts/ 目录里

**创建一个虚拟网卡配置**，注意名称必须以物理网卡名称为前缀
nano ifcfg-eth0:0
编辑内容：
DEVICE=eth0:0
BOOTPROTO=static
IPADDR=192.168.1.221
BROADCAST=192.168.1.255
NETMASK=255.255.255.0
ONBOOT=yes

IPADDR 可以定义为其他IP，但每一组多台服务器的 IPADDR 要一致，才能协作工作

**重启网络**
service network restart
CentOS 可能会没有 network 服务，需要用
nmcli c reload eth0
再不行就重启系统
直到 ifconfig 有 eth0:0 出现，才证明虚拟网卡在工作

## 安装其他所需网络服务

**我使用了 web 服务，多台机子安装 nginx**
yum install -y nginx

**启动 nginx 服务**
service nginx start

nginx 的网页文件在 /usr/share/nginx/html/

**复制首页备份**
cp index.html index.html.bak

**编辑 index.html**
找到 h1 标记，内容最后添加一些文字
主服务器添加 (keepalived master)
备份服务器添加 (keepalived backup)

**防火墙开放 http 服务**
firewall-cmd --add-service http
**刷新防火墙配置**
firewall-cmd --reload

假如是在虚拟机中试验，这个时候就可以关机，复制多一台机子，以下是各台 keepalived 服务器的分别配置。

## 配置 keepalived 服务

配置文件在 /etc/keepalived/keepalived.conf

多台服务器上，在公共配置 global_defs，配置电子邮箱

**主服务器（master）配置**
global_defs 段添加 nopreempt 属性，代表非抢占模式，当 master 因为宕机恢复过来后，重新当回 master
vrrp_instance 段的 interface 属性检查是否对应真实环境的物理网卡名称
vrrp_instance 段的 virtual_ipaddress 属性设置为虚拟网卡的 IP 地址

**备份服务器（backup）配置**
vrrp_instance 段的 state 属性设置为 BACKUP
vrrp_instance 段的 priority 属性设置 50，其实只要小于 master 的 priority 就行了
vrrp_instance 段的 interface 属性检查是否对应真实环境的物理网卡名称
vrrp_instance 段的 virtual_ipaddress 属性设置为虚拟网卡的 IP 地址

## 启动并检查 keepalived 服务状态

**多台服务器，启动服务**
service keepalived start

**查看服务状态**
service keepalived status

服务处于 active 状态就代表正常工作

假如启动失败，可以查看 /var/log/messages 日志

假如日志内容太多，可以先清空
echo '' > /var/log/messages

## 错误处理

**Can‘t open PID file**
有残留进程未正常退出，锁定了文件，需要结束进程
ps -aux | grep keepalived
kill <对应的进程ID>
假如是由于配置参数有误，会具体指明第几行，什么参数名的

## 检查工作效果

我使用的是 web 服务，每台服务器单独对自己做web请求检查
curl <物理网卡IP>
正常是返回 html 文件的内容
假如超时，很大可能是被防火墙拦截了，检查防火墙配置，或临时关闭防火墙
假如出现 Connection refused，则代表 nginx 没有正常工作，检查配置，尤其是端口
假如出现 404 Not Found，则代表 nginx 目录下缺少 index.html 文件

**客户端在浏览器使用虚拟IP访问**
正常是显示 nginx 默认网页，并且看到 (keepalived master) 的字样
假如超时，则检查客户端和服务器是否在同一个网络，可以用 ping 检查，ping 不通就检查网络配置

**检验高可用性**
keepalived master 服务器关机，浏览器刷新页面（继续使用虚拟IP访问），正常情况下，几秒钟后 keepalived master 的字样就会变成 keepalived backup 的字样

重新开启 keepalived master 服务器，浏览器刷新页面（继续使用虚拟IP访问），正常情况下，几秒钟后 keepalived backup 的字样又重新变回 keepalived  master 的字样了
