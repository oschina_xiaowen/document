# npm 笔记 #



## npm 命令 ##

npm init

初始化项目，创建一个 package.json 文件

npm install  根据 package.json 安装依赖
npm install <packageName> 安装指定的包

-S, -save  添加项目依赖，把模块和版本号添加到 package.json 的 dependencies 部分
-D, -save-dev  添加项目开发依赖，把模块和版本号添加到 package.json 的 devdependencies 部分

devdependencies  一般配置测试框架，编译时不需要用到的

## npm 常用第三方包 ##

mysql		MySQL server and client library
ws			Web Socket server and client library
sqlite3		SQLite 3 library
vue			Vue library
axios       web 客户端网络请求库
font-awesome  Web 前端图标库
moment        Web 前端时间库
md-to-pdf   MarkDown 文件转 pdf CLI 工具



## 运行程序失败解决方法

npm info package_name

查看版本，安装前一个版本

npm install package_name@vn-1



## 常见问题

1. request to https://registry.npmjs.org/tailwindcss/-/tailwindcss-3.4.10.tgz failed, reason: connect ETIMEDOUT 104.16.3.35:443

问题分析：常见于项目初始化阶段，可能是短时间大量请求外国资源，国内防火墙做了限速，导致访问超时

解决方法：可以尝试复制链接到浏览器访问，假如可以成功下载，则手动安装这个包

npm install tailwindcss

使得本地有这个包的缓存，然后重新执行 npm install

假如浏览器也无法访问，那可能是国内防火墙拦截了这个域名，这就需要更换另一个源





