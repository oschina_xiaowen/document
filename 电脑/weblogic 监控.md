# weblogic 监控

## 简介

weblogic 提供一个 Java 程序，运维人员可以通过命令行获取 weblogic 服务器状态信息和控制服务器

管理程序：/path/to/Oracle/Middleware/wlserver_10.3/server/lib/weblogic.jar

weblogic.jar 需要添加运行权限，路径全目录添加运行权限

## 获取管理服务器的状态

```shell
java –cp /path/to/Oracle/Middleware/wlserver_10.3/server/lib/weblogic.jar weblogic.Admin -adminurl t3:\\localhost:7001 –username weblogic –password weblogic123 GETSTATE
```

其中，-username 后面跟控制台用户，-password 后面跟控制台用户的密码

返回信息："AdminServer": RUNNING

## 获取被管理的服务器状态

```shell
java –cp /path/to/Oracle/Middleware/wlserver_10.3/server/lib/weblogic.jar weblogic.Admin -adminurl t3:\\localhost:7001 –username weblogic –password weblogic123 GETSTATE Server_1
```

返回信息："Server_1": RUNNING

获取被管理的服务器的健康状态

```shell
java –cp /path/to/Oracle/Middleware/wlserver_10.3/server/lib/weblogic.jar weblogic.Admin -adminurl t3:\\localhost:7001 –username weblogic –password weblogic123 get -pretty -mbean "$domain:Location=$ServerName,Name=$ServerName,Type=ServerRuntime" | grep OverallHealthState
```

包含 HEALTH_OK 代表健康，包含 HEALTH_OVERLOADED 代表过载，包含 HEALTH_WARN 代表警告，包含 HEALTH_CRITICAL 代表情况严重需要立即处理，包含 HEALTH_FAILED 代表失败

更多参考：https://docs.oracle.com/cd/E68505_01/wls/WLAPI/weblogic/health/HealthState.html


